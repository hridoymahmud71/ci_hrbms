<!DOCTYPE html>
<base href="<?= BASE_URL ?>">
<html lang="en">
<head>
    <?= isset($meta) ? $meta : "" ?>
    <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title><?= isset($title) ? $title : 'Hotel Room Booking Management System' ?></title>

    <?= isset($headlink) ? $headlink : "" ?>
</head>
<body>

<!-- Page Container -->
<div class="page-container">
    <?= isset($content) ? $content : "" ?>
</div>
<!-- /Page Container -->


<?= isset($footlink) ? $footlink : "" ?>
</body>
</html>