<!DOCTYPE html>
<base href="<?= BASE_URL?>">
<html lang="en">
<head>
    <?= isset($meta) ? $meta : "" ?><?= $meta?>
    <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title><?= isset($title) ? $title : 'Hotel Room Booking Management System' ?></title>

    <?= isset($headlink) ? $headlink : "" ?>

</head>
<body>

<!-- Page Container -->
<div class="page-container">
    <!-- Page Sidebar -->
    <?= isset($sidebar) ? $sidebar : "" ?>
    <!-- /Page Sidebar -->

    <!-- Page Content -->
    <div class="page-content">
        <!-- Page Header -->
        <?= isset($navbar) ? $navbar : "" ?>
        <!-- /Page Header -->
        <!-- Page Inner -->
        <div class="page-inner">
            <?= isset($content) ? $content : "" ?>
            <?= isset($footer) ? $footer : "" ?>
        </div>
        <!-- /Page Inner -->

    </div><!-- /Page Content -->
</div><!-- /Page Container -->


<?= isset($footlink) ? $footlink : "" ?>
</body>
</html>