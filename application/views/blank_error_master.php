<!DOCTYPE html>
<base href="<?= BASE_URL?>">
<html lang="en">
<head>
    <?= isset($meta) ? $meta : "" ?>
    <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title><?= isset($title) ? $title : 'Hotel Room Booking Management System' ?></title>

    <?= isset($headlink) ? $headlink : "" ?>
</head>
<body class="error-page">
<!-- Page Content -->
<div class="page-container page-error">
    <div class="page-content">
        <!-- Page Inner -->
        <div class="page-inner">
            <?= isset($content) ? $content : "" ?>
        </div><!-- /Page Inner -->
    </div><!-- /Page Content -->
</div>


<?= isset($footlink) ? $footlink : "" ?>
</body>
</html>