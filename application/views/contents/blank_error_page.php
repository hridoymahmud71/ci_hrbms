<div id="main-wrapper" class="container">
    <div class="row">
        <div class="col-md-6 center">
            <h1 class="error-page-logo"><?= isset($blank_page_h1) ? $blank_page_h1 : "" ?></h1>
            <p class="error-page-top-text"><?= isset($blank_page_top_p) ? $blank_page_top_p : "" ?></p>
            <p class="error-page-bottom-text"><?= isset($blank_page_bottom_p) ? $blank_page_bottom_p : "" ?></p>
            <button href="index.html" onclick="goBack()" class="btn btn-default btn-lg m-b-xxs">Back</button>
        </div>
    </div><!-- Row -->
</div><!-- Main Wrapper -->

<script>
    function goBack() {
        window.history.back();
    }
</script>