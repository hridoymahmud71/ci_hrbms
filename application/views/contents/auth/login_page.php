<!-- Page Inner -->

<div class="page-inner login-page">
    <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger alert-dismissible" role="alert" style="margin-bottom:0;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <?= $this->session->flashdata('validation_errors') ? $this->session->flashdata('validation_errors') : "" ?>
            <?= $this->session->flashdata('email_not_found') ? $this->session->flashdata('email_not_found') : "" ?>
            <?= $this->session->flashdata('user_inactive') ? $this->session->flashdata('user_inactive') : "" ?>
            <?= $this->session->flashdata('password_mismatch') ? $this->session->flashdata('password_mismatch') : "" ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:0;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <?= $this->session->flashdata('activation_applied') ? $this->session->flashdata('activation_applied') : "" ?>
        </div>
    <?php } ?>
    <div id="main-wrapper" class="container-fluid">
        <div class="row">
            <div class="col-sm-6 col-md-3 login-box">
                <h4 class="login-title">Sign in to your account - <?= $panel ?></h4>
                <form method="post" action="<?= $action ?>" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="">Email address</label>
                        <input type="email" name="email" class="form-control" id="email"
                               value="<?= $this->session->flashdata('flash_email') ? $this->session->flashdata('flash_email') : "" ?>">
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" name="password" class="form-control" id="password" value="">
                    </div>
                    <button type="submit" class="btn btn-primary">Login</button>
                    <br>
                    <?php if ($panel == "Customer") { ?>
                        <a href="registration" class="forgot-link">Register as customer</a>
                    <?php } ?>
                    Or
                    <a href="<?= $forgot_password_url ?>" class="forgot-link">Forgot password?</a>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Page Content -->