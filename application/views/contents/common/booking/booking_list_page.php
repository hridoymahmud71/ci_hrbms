<div class="page-title">
    <h3 class="breadcrumb-header"><?= isset($title) ? $title :""?></h3>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <?php if ($this->session->flashdata('success')) { ?>
                <div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:0;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <?= $this->session->flashdata('email_send_success') ? $this->session->flashdata('email_send_success') : "" ?>
                    <?= $this->session->flashdata('approve_success') ? $this->session->flashdata('approve_success') : "" ?>
                </div>
            <?php } ?>
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">List</h4>
                </div>
                <div class="panel-body">

                    <div class="table-responsive">
                        <table id="table" class="display table" style="width: 100%; cellspacing: 0;">
                            <thead>
                            <tr>
                                <th>Booking Key</th>
                                <th>Customer</th>
                                <th>Customer Email</th>
                                <th>Amount</th>
                                <th>Paypal TxId</th>
                                <th>Approval</th>
                                <th>Last Updated At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Row -->
</div>
<!-- Main Wrapper -->

<script type="text/javascript" src="<?= $this->config->item('custom_js_path') ?>booking/booking_list.js"></script>