<div class="page-title">
    <h3 class="breadcrumb-header"><?= isset($title) ? $title : "" ?></h3>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="col-md-4" style="padding-left:0;">
                        <h3 class="m-b-md m-t-xxs"><b><?= $this->config->item('site_name') ?></b></h3>
                        <p><b>Paypal transaction Id:</b> <?= isset($booking_order['txn_id']) ? $booking_order['txn_id'] : "" ?><br></p>
                        <p><b>Booking order key:</b> <?= isset($booking_order['booking_order_key']) ? $booking_order['booking_order_key'] : "" ?><br></p>
                    </div>
                    <div class="col-md-8 text-right" style="padding-right:0;">
                        <h3 class="m-t-xs">Invoice/Order</h3>
                    </div>
                    <div class="col-md-12" style="padding-left:0;">
                        <hr>
                        <p>
                            <strong>Invoice/Order to</strong><br>
                            <?= isset($booking_order['user_name']) ? $booking_order['user_name'] : "" ?><br>
                            <?= isset($booking_order['user_email']) ? $booking_order['user_email'] : "" ?><br>

                        </p>
                    </div>
                    <div class="col-md-12" style="padding-left:0;padding-right:0;">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Item</th>
                                <th>Description</th>
                                <th>Quantity</th>
                                <th>Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($booking_order['booking_order_room_categories'])) { ?>
                                <?php if (!empty($booking_order['booking_order_room_categories'])) { ?>
                                    <?php foreach ($booking_order['booking_order_room_categories'] as $booking_order_room_category) { ?>
                                        <tr>
                                            <td><?= $booking_order_room_category['room_category_name']?></td>
                                            <td>No Description Available</td>
                                            <td><?= $booking_order_room_category['room_count']?></td>
                                            <td><?= $booking_order_room_category['room_category_price']?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-4 col-md-offset-8" style="padding-right:0;">
                        <div class="text-right">
                            <h4 class="no-m m-t-sm">Payment Status</h4>
                            <h2 class="no-m"><?= isset($booking_order['booking_order_is_paid']) ? ($booking_order['booking_order_is_paid'] == 1 ? "Paid" : "Unpaid") : "Unknown" ?></h2>
                            <hr>
                            <h4 class="no-m m-t-sm">Approval Status</h4>
                            <h2 class="no-m"><?= isset($booking_order['booking_order_approval']) ? ($booking_order['booking_order_approval'] == 1 ? "Approved" : "Not Approved") : "Unknown" ?></h2>
                            <hr>
                            <h4 class="no-m m-t-md text-success">Total</h4>
                            <h1 class="no-m text-success"><?= isset($booking_order['currency_code']) ? $booking_order['currency_code'] : "0.00" ?><?= isset($booking_order['booking_order_amount']) ? $booking_order['booking_order_amount'] : "0.00" ?></h1>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Row -->
</div><!-- Main Wrapper -->