<div class="page-title">
    <?php $panel_name = "User - Dashboard";

    if ($this->user->is_admin()) {
        $panel_name = "Admin - Dashboard";
    } else if ($this->user->is_customer()) {
        $panel_name = "Customer - Dashboard";
    }
    ?>
    <h3 class="breadcrumb-header"><?= $panel_name ?></h3>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <?php if (!$this->user->is_logged_in()) { ?>
                <div class="panel panel-white">
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title">You are not logged in</h4>
                    </div>
                    <div class="panel-body text-center">
                        <p class="text">
                            Login as customer
                        </p>
                        <p>
                            <a type="button" href="login" class="btn btn-primary btn-lg">Customer Login</a>
                        </p>
                        <p class="text-muted">Or</p>
                        <p>
                            Are you administrator?
                        </p>
                        <p>
                            <a type="button" href="admin/login" class="btn btn-default btn-sm">Admin Login</a>
                        </p>

                    </div>
                </div>
            <?php } ?>

            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">
                        Hello <?= $this->user->get_user_name(false, true) ? $this->user->get_user_name(false, true) : "guest" ?>
                        ,</h4>
                </div>
                <div class="panel-body text-center">

                    <p>
                        <a type="button" href="room-category-list" class="btn btn-default btn-rounded btn-lg">See all
                            kinds of rooms!</a>
                    </p>
                    <?php if ($this->user->is_logged_in()) { ?>
                        <p>
                            <?= $this->user->is_customer() ? "Already Booked ? See your bookings" : "See what customers are booking..." ?></a>
                        </p>
                        <p>
                            <a type="button" href="booking-list"
                               class="btn btn-default btn-sm"><?= $this->user->is_admin() ? "All" : "My" ?> Bookings</a>
                        </p>
                    <?php } ?>

                </div>
            </div>

        </div>
    </div><!-- Row -->

</div>
<!-- Main Wrapper -->