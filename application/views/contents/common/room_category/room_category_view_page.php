<div class="page-title">
    <h3 class="breadcrumb-header">Room Category Description</h3>
</div>

<div id="main-wrapper">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title"></h4>
                </div>


                <div class="panel-body">

                    <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                        <ul class="pricing">
                            <li>
                                <p class="text-muted">Room Category</p>
                                <h1><?= isset($room_category['room_category_name']) ? $room_category['room_category_name'] : "" ?></h1>
                            </li>
                            <li>
                                <p class="text-muted">Total Rooms</p>
                                <?= isset($room_category['total_number_of_rooms_in_category']) ? $room_category['total_number_of_rooms_in_category'] : "" ?>
                            </li>
                            <li>
                                <img class="img-thumbnail"
                                     src="<?= isset($room_category['room_category_featured_image']) ? $room_category['room_category_featured_image']['room_category_featured_image_name_with_path'] : "" ?>"
                                     alt="photo">
                            </li>
                            <li>
                                <p class="text-muted">Price</p>
                                <h3 style="margin-top:0;">
                                    $<?= isset($room_category['room_category_price']) ? $room_category['room_category_price'] : "" ?></h3>
                            </li>
                            <?php if ($this->user->is_admin()) { ?>
                                <li>
                                    <a href="room-category-edit/<?= isset($room_category['room_category_key']) ? $room_category['room_category_key'] : "" ?>"
                                       class="btn btn-warning btn-rounded">Edit</a>
                                </li>
                            <?php } ?>
                            <?php if ($this->user->is_customer()) { ?>
                                <li>
                                    <a href="book?pre_selected_room_category=<?= isset($room_category['room_category_key']) ? $room_category['room_category_key'] : "" ?>"
                                       class="btn btn-success btn-rounded">Book</a>
                                </li>
                            <?php } ?>
                            <?php if (!$this->user->is_logged_in()) { ?>
                                <li>
                                    <span>Please <a href="login" type="button"
                                                    class="btn btn-default btn-sm btn-rounded"> Log in</a> in order to book this</span>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="panel panel-white">
                <h4 class="panel-title">Description</h4>
                <div class="panel-body panel-collapse">
                    <?= isset($room_category['room_category_description']) ? $room_category['room_category_description'] : "" ?>
                </div>
            </div>

            <?php if (isset($room_category['room_category_images'])) { ?>
                    <?php if (!empty($room_category['room_category_images'])) { ?>
                    <div class="panel panel-white">
                        <h4 class="panel-title">Images</h4>
                        <div class="row">

                            <div id="grid-gallery" class="grid-gallery">
                                <section class="grid-wrap">
                                    <ul class="grid">
                                        <li class="grid-sizer"></li>
                                        <?php foreach ($room_category['room_category_images'] as $room_category_image) { ?>
                                            <li>
                                                <figure>
                                                    <img src="<?= $room_category_image['room_category_image_name_with_path'] ?>"
                                                         alt="photo"/>
                                                </figure>
                                            </li>
                                        <?php } ?>

                                    </ul>
                                </section>
                                <section class="slideshow">
                                    <ul>
                                        <?php foreach ($room_category['room_category_images'] as $room_category_image) { ?>
                                            <li>
                                                <figure>
                                                    <img src="<?= $room_category_image['room_category_image_name_with_path'] ?>"
                                                         alt="photo"/>
                                                </figure>
                                            </li>
                                        <?php } ?>
                                    </ul>

                                    <nav>
                                        <span class="fa fa-angle-left nav-prev"></span>
                                        <span class="fa fa-angle-right nav-next"></span>
                                        <span class="icon-close nav-close"></span>
                                    </nav>
                                </section>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php } ?>





        </div>

        <!-- Main Wrapper -->


        <script type="text/javascript"
                src="<?= $this->config->item('custom_js_path') ?>room_category/room_category_view.js"></script>