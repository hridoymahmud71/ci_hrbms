<div class="page-title">
    <h3 class="breadcrumb-header"><?= $form_title ?></h3>
</div>

<div id="main-wrapper">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title"><?= strtoupper($which_form) ?> Details</h4>
                </div>
                <?php if ($this->session->flashdata('error')) { ?>
                    <div class="alert alert-danger alert-dismissible" role="alert" style="margin-bottom:0;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <?= $this->session->flashdata('validation_errors') ? $this->session->flashdata('validation_errors') : "" ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:0;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <?= $this->session->flashdata('add_success') ? $this->session->flashdata('add_success') : "" ?>
                        <?= $this->session->flashdata('update_success') ? $this->session->flashdata('update_success') : "" ?>
                        <a href="room-category-list">All Room Categories</a>
                    </div>
                <?php } ?>
                <hr>
                <div class="panel-body">
                    <form id="form" action="<?= $form_action ?>" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="room_category_key"
                               value="<?= isset($room_category_key) ? $room_category_key : "" ?>">
                        <div class="form-group">
                            <label for="">Room Category Name</label>
                            <input type="text" class="form-control" id="" name="room_category_name"
                                   value="<?= isset($room_category_name) ? $room_category_name : "" ?>"
                                   placeholder="Room Category Name">
                        </div>
                        <div class="form-group">
                            <label for="">Room Category Description</label>
                            <textarea class="form-control summernote" id=""
                                      name="room_category_description"><?= isset($room_category_description) ? $room_category_description : "" ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Room Category Price</label>
                            <input type="number" min="0" step=".01" class="form-control" id=""
                                   name="room_category_price"
                                   placeholder="Room Category Price"
                                   value="<?= isset($room_category_price) ? $room_category_price : "" ?>">
                        </div>
                        <div class="form-group">
                            <label for="">Available Rooms</label>
                            <input type="number" min="0" step="1" class="form-control" id=""
                                   name="total_number_of_rooms_in_category" placeholder="Available Rooms"
                                   value="<?= isset($total_number_of_rooms_in_category) ? $total_number_of_rooms_in_category : "" ?>">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
            <div style="display: none" id="room_catgory_image_upload_url" url="room-category-image-upload"></div>

            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">Images</h4>
                </div>
                <div class="panel-body">
                    <div class="dropzone">
                        <div class="dz-message">
                            <h4 class="text-center text-muted"> Click Here to upload Images </h4>
                            <p class="text-center text-muted">(Press submit button above once uploaded)</p>
                        </div>

                    </div>
                    <div class="previews" id="preview"></div>
                </div>
                <div>* Max 2 MB/file upto 10 files</div>
            </div>

            <?php if (isset($room_category_images)) { ?>
                <?php if (!empty($room_category_images)) { ?>
                    <div class="panel panel-white">
                        <h4 class="panel-title">Images</h4>
                        <div class="row">
                            <?php foreach ($room_category_images as $room_category_image) { ?>
                                <div class="col-md-4 image_block">
                                    <div class="thumbnail">
                                        <img src="<?= $room_category_image['room_category_image_name_with_path'] ?>" class="" alt="photo">
                                        <div class="caption text-center">
                                            <button class="btn btn-danger image_delete_btn" room_category_image_id="<?= $room_category_image['room_category_image_id']?>">Delete</button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>


        </div>


    </div>


</div>
<style>
    .dz-error-mark, .dz-success-mark, .dz-remove {
        display: none;
    }
</style>
<!-- Main Wrapper -->


<script type="text/javascript"
        src="<?= $this->config->item('custom_js_path') ?>room_category/room_category_form.js"></script>