<div class="page-title">
    <h3 class="breadcrumb-header">Room Categories</h3>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">List</h4>
                </div>
                <div>
                    <h3 class="box-title">
                        <?php if ($this->user->is_admin()) { ?>
                        <a href="room-category-add" class="btn btn-success""><i
                                class="glyphicon glyphicon-plus"></i> Add Room Category
                        </a>
                        <?php } ?>
                    </h3>
                </div>
                <div class="panel-body">
                    <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:0;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <?= $this->session->flashdata('delete_success') ? $this->session->flashdata('delete_success') : "" ?>
                        </div>
                    <?php } ?>
                    <div class="table-responsive">
                        <table id="table" class="display table" style="width: 100%; cellspacing: 0;">
                            <thead>
                            <tr>
                                <th>Room Category</th>
                                <th>Available Rooms</th>
                                <th>Price</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($room_categories)) { ?>
                                <?php foreach ($room_categories as $room_category) { ?>
                                    <tr>
                                        <td><?= $room_category['room_category_name'] ?></td>
                                        <td><?= $room_category['total_number_of_rooms_in_category'] ?></td>
                                        <td><?= $room_category['room_category_price'] ?></td>
                                        <td>
                                            <img class="td-image"
                                                 src="<?= $room_category['room_category_featured_image']['room_category_featured_image_name_with_path'] ?>"
                                                 alt="photo">
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-primary"
                                               href="room-category/<?= $room_category['room_category_key'] ?>">View</a>
                                            <?php if ($this->user->is_admin()) { ?>
                                                <a class="btn btn-sm btn-warning"
                                                   href="room-category-edit/<?= $room_category['room_category_key'] ?>">Edit</a>
                                                <a class="btn btn-sm btn-danger"
                                                   href="room-category-delete/<?= $room_category['room_category_key'] ?>">Delete</a>
                                            <?php } ?>
                                            <?php if ($this->user->is_customer()) { ?>
                                                <a class="btn btn-sm btn-primary"
                                                   href="book?pre_selected_room_category=<?= isset($room_category['room_category_key']) ? $room_category['room_category_key'] : "" ?>">Book</a>
                                            <?php } ?>
                                            <?php if (!$this->user->is_logged_in()) { ?>
                                                <a class="btn btn-sm btn-primary"
                                                   href="login">Log in to book</a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Row -->
</div>
<!-- Main Wrapper -->
<script type="text/javascript"
        src="<?= $this->config->item('custom_js_path') ?>room_category/room_category_list.js"></script>