<div class="page-title">

    <h3 class="breadcrumb-header">Payment Successful</h3>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">

            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title"><?= isset($item_name) ? $item_name : "" ?></h4>
                </div>
                <div class="panel-body text-center">
                    <p class="text-muted">Payment Details below:</p>
                    <p class="text"><b>Booking:</b> <?= isset($item_number) ? $item_number : "" ?></p>
                    <p class="text"><b>Transaction: </b><?= isset($txn_id) ? $txn_id : "" ?></p>
                    <p class="text"><b>Amount:</b> <?= isset($payment_amt) ? $payment_amt : "" ?></p>
                    <p class="text"><b>Currency:</b> <?= isset($currency_code) ? $currency_code : "" ?></p>
                    <p class="text"><b>Status:</b> <?= isset($status)? $status : "" ?></p>
                </div>
            </div>


            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">
                        Hello <?= $this->user->get_user_name(false, true) ? $this->user->get_user_name(false, true) : "guest" ?>
                        ,</h4>
                </div>
                <div class="panel-body text-center">

                    <p>
                        <a type="button" href="room-category-list" class="btn btn-default btn-rounded btn-lg">See all
                            kinds of rooms!</a>
                    </p>
                    <?php if ($this->user->is_logged_in()) { ?>
                        <p>
                            <?= $this->user->is_customer() ? "Already Booked ? See your bookings" : "See what customers are booking..." ?></a>
                        </p>
                        <p>
                            <a type="button" href="booking-list"
                               class="btn btn-default btn-sm"><?= $this->user->is_admin() ? "All" : "My" ?> Bookings</a>
                        </p>
                    <?php } ?>

                </div>
            </div>

        </div>
    </div><!-- Row -->

</div>
<!-- Main Wrapper -->