<div class="page-title">

    <h3 class="breadcrumb-header">Book A Page</h3>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12 ">

            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">Select A Room Type</h4>
                </div>
                <div class="panel-body text-center">
                    <div class="row room_categories" id="room_categories" pre_selected_room_category="<?= $pre_selected_room_category?>">
                        <?php if (!empty($room_categories)) { ?>
                            <?php foreach ($room_categories as $room_category) { ?>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="text-center">
                                            <img class="book_room_cat_image"
                                                 src="<?= $room_category['room_category_featured_image']['room_category_featured_image_name_with_path'] ?>"
                                                 alt="Card image cap">
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title"><?= $room_category['room_category_price'] ?></h5>
                                        </div>
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item"><?= $room_category['room_category_name'] ?></li>
                                            <li class="list-group-item">Available: x out
                                                of <?= $room_category['total_number_of_rooms_in_category'] ?></li>
                                            <li class="list-group-item">
                                                <a href="#"
                                                   id="<?= $room_category['room_category_key'] ?>"
                                                   data-name="<?= $room_category['room_category_name'] ?>"
                                                   data-key="<?= $room_category['room_category_key'] ?>"
                                                   data-price="<?= $room_category['room_category_price'] ?>"
                                                   class="add-to-cart btn btn-primary">Add to cart
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <nav class="navbar navbar-inverse bg-inverse navbar-fixed-bottom bg-faded">
                <div class="row">
                    <div class="col">
                        <div class="text-center">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cart">Cart (<span
                                        class="total-count"></span>)
                            </button>
                            <button class="clear-cart btn btn-danger">Clear Cart</button>
                        </div>
                    </div>
                </div>
            </nav>

            <!-- Modal -->
            <div class="modal fade" id="cart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Cart</h5>
                            <span style="color:darkred;" id="response-message-error"></span>
                            <span style="color:darkgreen;" id="response-message-success"></span>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <table class="show-cart table">

                            </table>
                            <div>Total price: <span class="total-cart"></span></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button id="order" type="button" class="btn btn-primary">Order now</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div><!-- Row -->

</div>
<!-- Main Wrapper -->

<script type="text/javascript"
        src="<?= $this->config->item('custom_js_path') ?>booking/book.js"></script>