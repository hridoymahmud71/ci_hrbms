<!-- Javascripts -->
<script src="<?= $this->config->item('asset_path')?>plugins/jquery/jquery-3.1.0.min.js"></script>
<script src="<?= $this->config->item('asset_path')?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= $this->config->item('asset_path')?>plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?= $this->config->item('asset_path')?>plugins/uniform/js/jquery.uniform.standalone.js"></script>

<script src="<?= $this->config->item('asset_path')?>plugins/datatables/js/jquery.datatables.min.js"></script>
<script src="<?= $this->config->item('asset_path')?>plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script src="<?= $this->config->item('asset_path')?>plugins/switchery/switchery.min.js"></script>
<script src="<?= $this->config->item('asset_path')?>plugins/summernote-master/summernote.min.js"></script>
<script src="<?= $this->config->item('asset_path')?>plugins/dropzone/dropzone.min.js"></script>
<script src="<?= $this->config->item('asset_path')?>js/space.min.js"></script>

<script src="<?= $this->config->item('asset_path')?>plugins/slick-1.8.1/slick/slick.js"></script>

<script src="<?= $this->config->item('asset_path')?>plugins/gridgallery/js/imagesloaded.pkgd.min.js"></script>
<script src="<?= $this->config->item('asset_path')?>plugins/gridgallery/js/masonry.pkgd.min.js"></script>
<script src="<?= $this->config->item('asset_path')?>plugins/gridgallery/js/classie.js"></script>
<script src="<?= $this->config->item('asset_path')?>plugins/gridgallery/js/cbpgridgallery.js"></script>
