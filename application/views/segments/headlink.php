<!-- Styles -->
<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
<link href="<?= $this->config->item('asset_path') ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<?= $this->config->item('asset_path') ?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="<?= $this->config->item('asset_path') ?>plugins/icomoon/style.css" rel="stylesheet">
<link href="<?= $this->config->item('asset_path') ?>plugins/uniform/css/default.css" rel="stylesheet"/>
<link href="<?= $this->config->item('asset_path') ?>plugins/switchery/switchery.min.css" rel="stylesheet"/>

<link href="<?= $this->config->item('asset_path') ?>plugins/datatables/css/jquery.datatables.min.css" rel="stylesheet"
      type="text/css"/>
<link href="<?= $this->config->item('asset_path') ?>plugins/datatables/css/jquery.datatables_themeroller.css"
      rel="stylesheet" type="text/css"/>
<link href="<?= $this->config->item('asset_path') ?>plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet"
      type="text/css"/>

<link href="<?= $this->config->item('asset_path') ?>plugins/summernote-master/summernote.css" rel="stylesheet"
      type="text/css"/>
<link href="<?= $this->config->item('asset_path') ?>assets/plugins/dropzone/dropzone.min.css" rel="stylesheet">

<link href="<?= $this->config->item('asset_path') ?>plugins/gridgallery/css/component.css" rel="stylesheet">
<!-- Theme Styles -->
<link href="<?= $this->config->item('asset_path') ?>css/space.min.css" rel="stylesheet">
<link href="<?= $this->config->item('asset_path') ?>css/custom.css" rel="stylesheet">

<link href="<?= $this->config->item('asset_path') ?>plugins/slick-1.8.1/slick/slick.css" rel="stylesheet">
<link href="<?= $this->config->item('asset_path') ?>plugins/slick-1.8.1/slick/slick-theme.css" rel="stylesheet">

<script src="<?= $this->config->item('asset_path') ?>plugins/gridgallery/js/modernizr.custom.js"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

<![endif]-->