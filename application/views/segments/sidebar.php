<div class="page-sidebar">
    <a class="logo-box" href="<?= $this->user->is_admin()?"admin/dashboard":"dashboard"?>">
        <span>Space</span>
        <i class="icon-radio_button_unchecked" id="fixed-sidebar-toggle-button"></i>
        <i class="icon-close" id="sidebar-toggle-button-close"></i>
    </a>
    <div class="page-sidebar-inner">
        <div class="page-sidebar-menu">
            <ul class="accordion-menu">
                <li>
                    <a href="<?= $this->user->is_admin()?"admin/dashboard":"dashboard"?>">
                        <i class="menu-icon icon-home4"></i><span>Dashboard</span>
                    </a>
                </li>
                <?php if ($this->user->is_admin()) { ?>
                <li>
                    <a href="admin/customer-list">
                        <i class="menu-icon icon-user"></i><span>Customers</span>
                    </a>
                </li>
                <?php } ?>
                <li>
                    <a href="room-category-list">
                        <i class="menu-icon icon-drawer"></i><span>Room Categories</span>
                    </a>
                </li>
                <?php if ($this->user->is_logged_in()) { ?>
                <li>
                    <a href="booking-list">
                        <i class="menu-icon icon-monetization_on"></i><span>Room Bookings</span>
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>