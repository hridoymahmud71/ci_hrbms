<?php

class Auth_model extends CI_Model
{
    public $user_table = "user";
    public $role_table = "role";
    public $user_role_table = "user_role";
    public $user_activation_table = "user_activation";

    public $password_salt = "devilmaycry"; //unused


    public function __construct()
    {
        parent::__construct();
    }

    public function sess_destroy()
    {
        $this->session->sess_destroy();
    }

    public function get_role_row($role)
    {
        $this->db->select('*');
        $this->db->from($this->role_table);
        $this->db->where('role', $role);


        $query = $this->db->get();
        $row = $query->row_array();

        return $row;
    }

    public function get_role_id_by_role($role)
    {
        $role_id = 0;
        $role_row = $this->get_role_row($role);

        if(!empty($role_row)){
            $role_id = $role_row['role_id'];
        }

        return $role_id;
    }

    public function generate_and_insert_activation_code($user_id)
    {
        $activation_code = $this->important->generate_key('user_activation',' 	user_activation_code','usactv');

        $ins_data['user_id'] = $user_id;
        $ins_data['user_activation_code'] = $activation_code;
        $ins_data['user_activation_code_expires'] = date('Y-m-d', strtotime("+2 day")); //expires after 2 days
        $this->db->insert('user_activation', $ins_data);

        return $activation_code;
    }

    public function process_activation_code($activation_code)
    {
        $ret  = false;
        $act_row = $this->db->select('*')->from($this->user_activation_table)->where('user_activation_code', $activation_code)->get()->row_array();

        if(!empty($act_row)){
            $this->db->set('user_active', 1, FALSE);
            $this->db->where('user_id', $act_row['user_id']);
            $this->db->update($this->user_table);
            $ret  = true;

            $this->db->delete($this->user_activation_table, array('user_activation_code' => $activation_code)); //then delete it
        }

            return $ret;
    }

    public function set_user_id_in_session($user_id)
    {
        $data['user_id'] = $user_id;
        $this->session->set_userdata($data);
    }

    public function password_hash($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public function password_verify($password,$hash)
    {
        return password_verify($password, $hash);
    }


}