<?php

class User_model extends CI_Model
{
    public $user_table = "user";
    public $role_table = "role";
    public $user_role_table = "user_role";

    public function __construct()
    {
        parent::__construct();
    }

    public function create_user($user_data, $role)
    {
        $return = array();

        $user_data['user_key'] = $this->important->generate_key($this->user_table, 'user_key', 'usr');
        $user_data['user_password'] = $this->auth->password_hash($user_data['user_password']);
        $user_data['user_created_at'] = date("Y-m-d H:i:s");

        $this->db->insert($this->user_table, $user_data);

        $user_id = $this->db->insert_id();

        $return['user_id'] = $user_id;
        $return['role_id'] = 0;
        $return['user_role_id'] = 0;

        $role_row = $this->auth->get_role_row($role);

        if (!empty($role_row)) {
            $r = array();
            $r['user_id'] = $user_id;
            $r['role_id'] = $role_row['role_id'];


            $user_role_id = $this->db->insert($this->user_role_table, $r);

            $return['role_id'] = $role_row['role_id'];
            $return['user_role_id'] = $user_role_id;
        }

        return $return;
    }

    public function admin_permitted_or_thrown_out()
    {
        if (!$this->is_logged_in()) {
            redirect('admin/login');
        }


        if (!$this->is_admin()) {
            redirect('permission-denied');
        }
    }

    public function customer_permitted_or_thrown_out()
    {
        if (!$this->is_logged_in()) {
            redirect('login');
        }


        if (!$this->is_customer()) {
            redirect('permission-denied');
        }
    }




    public function is_admin($user_id = false)
    {
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id') ? $this->session->userdata('user_id') : 0;
        }

        return $this->has_role('admin', $user_id);
    }

    public function is_customer($user_id = false)
    {
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id') ? $this->session->userdata('user_id') : 0;
        }

        return $this->has_role('customer', $user_id);
    }


    public function has_role($role, $user_id = false)
    {
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id') ? $this->session->userdata('user_id') : 0;
        }

        $ret = $this->if_user_role_exists($role, $user_id);

        return $ret;
    }

    public function if_user_role_exists($role, $user_id)
    {
        $this->db->select('*');
        $this->db->from("{$this->user_role_table} as ur");
        $this->db->join("{$this->role_table} as r", "ur.role_id=r.role_id");
        $this->db->where('r.role', $role);
        $this->db->where('ur.user_id', $user_id);
        $this->db->limit(1);

        $num_rows = $this->db->get()->num_rows();
        return $num_rows > 0 ? true : false;
    }

    public function if_user_email_exists($user_email)
    {
        $this->db->select('*');
        $this->db->from($this->user_table);

        $this->db->where('user_email', $user_email);
        $this->db->limit(1);

        $num_rows = $this->db->get()->num_rows();
        return $num_rows > 0 ? true : false;
    }

    public function get_user_ids_by_role($role)
    {
        $ids = array(0);

        $this->db->select('ur.user_id');
        $this->db->from("{$this->user_role_table} as ur");
        $this->db->join("{$this->role_table} as r", "ur.role_id=r.role_id", 'left');
        $this->db->where('r.role', $role);


        $rows = $this->db->get()->result_array();

        if (!empty($rows)) {
            $ids = array_column($rows, 'user_id');
            $ids = array_unique($ids);
        }

        return $ids;

    }
    public function get_users_by_role($role)
    {
        $this->db->select('*');
        $this->db->from("{$this->user_role_table} as ur");
        $this->db->join("{$this->role_table} as r", "ur.role_id=r.role_id", 'left');
        $this->db->join("{$this->user_table} as u", "ur.user_id=u.user_id");
        $this->db->where('r.role', $role);


        $rows = $this->db->get()->result_array();

        return $rows;

    }


    public function get_user_ids_by_role_subquery($role)
    {
        $ids = array(0);

        $this->db->select('ur.user_id');
        $this->db->from("{$this->user_role_table} as ur");
        $this->db->join("{$this->role_table} as r", "ur.role_id=r.role_id", 'left');
        $this->db->where('r.role', $role);


        $query = $this->db->get();

        return $this->db->last_query();

    }

    public function is_logged_in()
    {
        $bool = $this->session->userdata('user_id') ? true : false;
        return $bool;
    }

    public function get_user_by_email($email)
    {
        $this->db->select('*');
        $this->db->from($this->user_table);
        $this->db->where('user_email', $email);

        return $this->db->get()->row_array();
    }

    public function get_user_by_id($user_id)
    {
        $this->db->select('*');
        $this->db->from($this->user_table);
        $this->db->where('user_id', $user_id);

        return $this->db->get()->row_array();
    }

    public function get_user_name($user_id, $only_first_name = false)
    {
        $name = "";

        if (!$user_id) {
            $user_id = $this->session->userdata('user_id') ? $this->session->userdata('user_id') : 0;
        }
        $user = $this->get_user_by_id($user_id);

        if (!empty($user)) {
            $name = $user['user_name'];

            if ($only_first_name) {
                $name = $this->get_first_name_from_user_name($name);
            }
        }

        return $name;
    }

    public function get_first_name_from_user_name($name)
    {
        $first_name = "";

        if (!empty($name)) {
            $exp_nm_arr = explode(' ', $name);

            if (isset($exp_nm_arr[0])) {
                $first_name = $exp_nm_arr[0];
            }
        }

        return $first_name;

    }

    public function get_user_by_key($key)
    {
        $this->db->select('*');
        $this->db->from($this->user_table);
        $this->db->where('user_key', $key);

        return $this->db->get()->row_array();
    }

}