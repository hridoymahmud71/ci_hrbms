<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_model extends CI_Model
{

    public $user_table = "user";
    public $role_table = "role";
    public $user_role_table = "user_role";

    var $column_order = array('user_name', 'user_email', 'user_active', 'user_created_at','user_updated_at'); //set column field database for datatable orderable
    var $column_search = array('user_name', 'user_email', 'user_active', 'user_created_at','user_updated_at'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('user_id' => 'desc'); // default order


    public function __construct()
    {
        parent::__construct();
        $this->order = array('user_created_at' => 'desc'); // latest on top
    }

    private function _get_datatables_query()
    {
        //$user_ids = $this->user->get_user_ids_by_role('customer');
        $user_ids_subquery = $this->user->get_user_ids_by_role_subquery('customer');

        $this->db->select('*');
        $this->db->from($this->user_table);
        $this->db->where("user_deleted !=",1);

        //$this->db->where_in('user_id',$user_ids);
        $this->db->where("user_id IN ({$user_ids_subquery})", NULL, FALSE);


        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result_array();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        //$user_ids = $this->user->get_user_ids_by_role('customer');
        $user_ids_subquery = $this->user->get_user_ids_by_role_subquery('customer');

        $this->db->select('*');
        $this->db->from($this->user_table);

        //$this->db->where_in('user_id',$user_ids);
        $this->db->where("user_id IN ({$user_ids_subquery})", NULL, FALSE);
        $this->db->where("user_deleted !=",1);

        $query = $this->db->get();
        return $query->num_rows();

        //return $this->db->count_all_results();
    }

    public function get_by_id($id)
    {
        $this->db->from($this->user_table);
        $this->db->where('user_id', $id);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function save($data)
    {
        $this->db->insert($this->user_table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update($this->user_table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id)
    {
        $this->db->where('user_id', $id);
        $this->db->delete($this->user_table);
    }



}