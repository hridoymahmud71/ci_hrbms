<?php

class Booking_model extends CI_Model
{
    public $user_table = "user";
    public $booking_order_table = "booking_order";
    public $booking_order_room_category_table = "booking_order_room_category";
    public $room_category_table = "room_category";
    public $paypal_transaction_table = "paypal_transaction";

    var $column_order = array('booking_order_key', 'user_name', 'user_email', 'payment_gross', 'txn_id', 'booking_order_approval', 'booking_order_updated_at'); //set column field database for datatable orderable
    var $column_search = array('booking_order_key', 'user_name', 'user_email', 'payment_gross', 'txn_id', 'booking_order_approval', 'booking_order_updated_at'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('booking_order_updated_at' => 'desc'); // default order

    var $user_table_alias = "u";
    var $booking_order_table_alias = "bo";
    var $paypal_transaction_table_alias = "pt";

    var $user_table_cols = array('user_name', 'user_email');
    var $booking_order_table_cols = array('booking_order_key', 'booking_order_approval', 'booking_order_updated_at');
    var $paypal_transaction_table_cols = array('payment_gross', 'txn_id');


    private function concate_col_alias($colname)
    {
        $str = $colname;

        if (in_array($colname, $this->user_table_cols)) {
            $str = $this->user_table_alias . '.' . $colname;
        }
        if (in_array($colname, $this->booking_order_table_cols)) {
            $str = $this->booking_order_table_alias . '.' . $colname;
        }
        if (in_array($colname, $this->paypal_transaction_table_cols)) {
            $str = $this->paypal_transaction_table_alias . '.' . $colname;
        }

        return $str;


    }

    private function _get_datatables_query($customer_id)
    {
        $this->db->select('*');
        $this->db->from("{$this->booking_order_table} as {$this->booking_order_table_alias}");
        $this->db->join("{$this->user_table} as {$this->user_table_alias}", "{$this->booking_order_table_alias}.user_id={$this->user_table_alias}.user_id");
        $this->db->join("{$this->paypal_transaction_table} as {$this->paypal_transaction_table_alias}", "{$this->booking_order_table_alias}.booking_order_key={$this->paypal_transaction_table_alias}.booking_order_key", "left");

        if ($customer_id > 0) {
            $this->db->where("{$this->user_table_alias}.user_id", $customer_id);
        }

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            $item = $this->concate_col_alias($item);


            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->concate_col_alias($this->column_order[$_POST['order']['0']['column']]), $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by($this->concate_col_alias(key($order)), $order[key($order)]);
        }
    }

    function get_datatables($customer_id)
    {
        $this->_get_datatables_query($customer_id);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result_array();
    }

    function count_filtered($customer_id)
    {
        $this->_get_datatables_query($customer_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($customer_id)
    {
        $this->db->select('*');
        $this->db->from("{$this->booking_order_table} as {$this->booking_order_table_alias}");
        $this->db->join("{$this->user_table} as {$this->user_table_alias}", "{$this->booking_order_table_alias}.user_id={$this->user_table_alias}.user_id");
        $this->db->join("{$this->paypal_transaction_table} as {$this->paypal_transaction_table_alias}", "{$this->booking_order_table_alias}.booking_order_key={$this->paypal_transaction_table_alias}.booking_order_key", "left");

        if ($customer_id > 0) {
            $this->db->where("{$this->user_table_alias}.user_id", $customer_id);
        }


        $query = $this->db->get();
        return $query->num_rows();

        //return $this->db->count_all_results();
    }

    //------------------------------------------------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

    public function get_booking_orders($limit, $is_paid)
    {
        $booking_orders = $this->__get_booking_orders($limit,$is_paid);
        if (!empty($booking_orders)) {
            $booking_orders = array_map("self::put_related_items_in_booking_order", $booking_orders);
        }

        return $booking_orders;
    }

    private function __get_booking_orders($limit, $is_paid)
    {
        $this->db->select('*');
        $this->db->from($this->booking_order_table);
        $this->db->where('booking_order_is_paid', $is_paid);

        if ($limit) {
            $this->db->limit($limit);
        }

        $query = $this->db->get();


        $rows = $query->result_array();

        return $rows;
    }

    public function put_related_items_in_booking_order($booking_order)
    {
        if (!empty($booking_order)) {
            $booking_order['booking_order_room_categories'] = $this->get_booking_order_room_categoies($booking_order['booking_order_id']);
        }

        return $booking_order;
    }

    public function get_booking_order_room_categoies($booking_order_id)
    {
        $this->db->select('*');
        $this->db->from("{$this->booking_order_room_category_table} as borc");
        $this->db->join("{$this->room_category_table} as rc", 'borc.room_category_id=rc.room_category_id');
        $this->db->where('booking_order_id ', $booking_order_id);

        $query = $this->db->get();
        $row = $query->result_array();

        return $row;
    }


    //------------------------------------------------------------------------------------------------------------------

    public function create_booking_order($booking_order, $booking_order_room_categories)
    {
        $booking_order['booking_order_key'] = $this->important->generate_key($this->booking_order_table, 'booking_order_key', 'bk_ord');
        $booking_order['booking_order_created_at'] = date("Y-m-d H:i:s");

        $this->db->insert($this->booking_order_table, $booking_order);

        $booking_order_id = $this->db->insert_id();

        if (isset($booking_order_room_categories)) {
            if (!empty($booking_order_room_categories)) {
                foreach ($booking_order_room_categories as $booking_order_room_category) {
                    $booking_order_room_category['booking_order_id'] = $booking_order_id;
                    $this->db->insert($this->booking_order_room_category_table, $booking_order_room_category);
                }
            }
        }

        return $booking_order['booking_order_key'];

    }

    public function update_booking_order($booking_order, $booking_order_booking_categories, $booking_order_key)
    {
        $booking_order['booking_order_updated_at'] = date("Y-m-d H:i:s");

        $this->db->update($this->booking_order_table, $booking_order, array('booking_order_key' => $booking_order_key));

        $booking_order_id = $this->get_booking_order_id_by_key($booking_order_key);

        if (isset($room_category_images) && $booking_order_id > 0) {
            if (!empty($booking_order_booking_categories)) {
                foreach ($room_category_images as $image) {
                    foreach ($booking_order_booking_categories as $booking_order_booking_category) {
                        $booking_order_booking_category['room_category_id'] = $booking_order_id;
                        $this->db->insert($this->booking_order_room_category_table, $booking_order_booking_category);
                    }
                }
            }

        }

        return $booking_order_key;

    }

    public function insert_paypal_transaction($paypal_transaction)
    {
        $booking_order['paypal_transaction_created_at'] = date("Y-m-d H:i:s");

        $this->db->insert($this->paypal_transaction_table, $paypal_transaction);
    }

    public function get_booking_order_id_by_key($booking_order_key)
    {
        $id = 0;
        $booking_order = $this->get_booking_order_by_key($booking_order_key);

        if (!empty($booking_order)) {
            $id = $booking_order['booking_order_key'];
        }

        return $id;
    }

    public function get_booking_order_by_key($booking_order_key)
    {
        $this->db->select('*');
        $this->db->from($this->booking_order_table);
        $this->db->where('booking_order_key', $booking_order_key);
        $query = $this->db->get();

        $row = $query->row_array();
        return $row;
    }

    public function get_detailed_booking_order_by_key($booking_order_key)
    {
        $booking_order = $this->__get_detailed_booking_order_by_key($booking_order_key);

        if (!empty($booking_order)) {
            $booking_order = $this->put_related_items_in_booking_order($booking_order);
        }
        return $booking_order;
    }

    public function __get_detailed_booking_order_by_key($booking_order_key)
    {
        $this->db->select('*');
        $this->db->from("{$this->booking_order_table} as {$this->booking_order_table_alias}");
        $this->db->join("{$this->user_table} as {$this->user_table_alias}", "{$this->booking_order_table_alias}.user_id={$this->user_table_alias}.user_id");
        $this->db->join("{$this->paypal_transaction_table} as {$this->paypal_transaction_table_alias}", "{$this->booking_order_table_alias}.booking_order_key={$this->paypal_transaction_table_alias}.booking_order_key", "left");


        $this->db->where("{$this->booking_order_table_alias}.booking_order_key", $booking_order_key);


        $query = $this->db->get();
        return $query->row_array();
    }


}