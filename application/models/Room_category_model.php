<?php

class Room_category_model extends CI_Model
{
    public $room_category_table = "room_category";
    public $room_category_image_table = "room_category_image";


    public function __construct()
    {
        parent::__construct();
    }

    public function get_room_categories($limit)
    {
        $room_categories = $this->__get_room_categories($limit);
        if (!empty($room_categories)) {
            $room_categories = array_map("self::put_related_items_in_room_category", $room_categories);
        }

        return $room_categories;
    }

    private function __get_room_categories($limit)
    {
        $this->db->select('*');
        $this->db->from($this->room_category_table);
        $this->db->where('room_category_deleted !=', 1);

        if ($limit) {
            $this->db->limit($limit);
        }

        $query = $this->db->get();


        $row = $query->result_array();

        return $row;
    }

    public function put_related_items_in_room_category($room_category)
    {
        if (!empty($room_category)) {
            $room_category['room_category_images'] = $this->get_room_category_images($room_category['room_category_id']);
            $room_category['room_category_featured_image'] = $this->get_room_category_featured_image($room_category['room_category_id']);
        }

        return $room_category;
    }

    public function get_room_category_images($room_category_id)
    {
        $room_category_images = $this->__get_room_category_images($room_category_id);
        if (!empty($room_category_images)) {
            $room_category_images = array_map("self::put_related_items_in_room_category_image", $room_category_images);
        }

        return $room_category_images;
    }

    private function __get_room_category_images($room_category_id)
    {
        $this->db->select('*');
        $this->db->from($this->room_category_image_table);
        $this->db->where('room_category_id', $room_category_id);

        $query = $this->db->get();

        $rows = $query->result_array();

        return $rows;
    }

    public function put_related_items_in_room_category_image($room_category_image)
    {
        $room_category_image_name = "";
        $room_category_image_name_with_path = $this->config->item("default_image_placeholder");
        if (!empty($room_category_image)) {
            if (!empty($room_category_image['room_category_image_name'])) {
                $room_category_image_name = $room_category_image['room_category_image_name'];
                $room_category_image_name_with_path = $this->config->item("room_category_image_source_path") . $room_category_image['room_category_image_name'];
            }
        }

        $room_category_image['room_category_image_name'] = $room_category_image_name;
        $room_category_image['room_category_image_name_with_path'] = $room_category_image_name_with_path;


        return $room_category_image;
    }

    public function get_room_category_featured_image($room_category_id)
    {
        $room_category_featured_image_name = "";
        $room_category_featured_image_name_with_path = $this->config->item("default_image_placeholder");

        $room_category_featured_image = $this->__get_room_category_featured_image($room_category_id);


        if (empty($room_category_featured_image)) {
            $room_category_first_image = $this->__get_room_category_first_image($room_category_id);

            if (!empty($room_category_first_image)) {
                $room_category_featured_image = $room_category_first_image;
            }
        }

        if (!empty($room_category_featured_image)) {
            if (!empty($room_category_featured_image['room_category_image_name'])) {
                $room_category_featured_image_name = $room_category_featured_image['room_category_image_name'];
                $room_category_featured_image_name_with_path = $this->config->item("room_category_image_source_path") . $room_category_featured_image['room_category_image_name'];
            }
        }

        $room_category_featured_image['room_category_featured_image_name'] = $room_category_featured_image_name;
        $room_category_featured_image['room_category_featured_image_name_with_path'] = $room_category_featured_image_name_with_path;

        return $room_category_featured_image;
    }

    private function __get_room_category_featured_image($room_category_id)
    {
        $this->db->select('*');
        $this->db->from($this->room_category_image_table);
        $this->db->where('room_category_id', $room_category_id);
        $this->db->where('is_image_featured', 1);

        $query = $this->db->get();

        $row = $query->row_array();

        return $row;
    }

    public function get_room_category_image_with_path($room_category_id, $file_path)
    {
        $image_with_path = "";
        $room_category_image = $this->get_room_category_image($room_category_id);
        if (!empty($room_category_image)) {
            $image_with_path =
                $file_path ? $this->config->item("room_category_image_upload_path") . $room_category_image['room_category_image_name'] : $this->config->item("room_category_image_source_path") . $room_category_image['room_category_image_name'];
        }

        return $image_with_path;
    }

    public function get_room_category_image($room_category_id)
    {
        $this->db->select('*');
        $this->db->from($this->room_category_image_table);
        $this->db->where('room_category_id', $room_category_id);

        $query = $this->db->get();

        $row = $query->row_array();

        return $row;
    }

    private function __get_room_category_first_image($room_category_id)
    {
        $this->db->select('*');
        $this->db->from($this->room_category_image_table);
        $this->db->where('room_category_id', $room_category_id);

        $query = $this->db->get();

        $row = $query->row_array();

        return $row;
    }

    //------------------------------------------------------------------------------------------------------------------

    public function create_room_category($room_category, $room_category_images)
    {
        $room_category['room_category_key'] = $this->important->generate_key($this->room_category_table, 'room_category_key', 'rm_cat');
        $room_category['room_category_created_at'] = date("Y-m-d H:i:s");

        $this->db->insert($this->room_category_table, $room_category);

        $room_category_id = $this->db->insert_id();

        if (isset($room_category_images)) {
            if (!empty($room_category_images)) {
                foreach ($room_category_images as $image) {
                    $room_category_image = array();
                    $room_category_image['room_category_id'] = $room_category_id;
                    $room_category_image['room_category_image_name'] = $image;
                    $this->db->insert($this->room_category_image_table, $room_category_image);
                }
            }
        }

        return $room_category['room_category_key'];

    }


    public function get_room_category_by_key($room_category_key)
    {
        $this->db->select('*');
        $this->db->from($this->room_category_table);
        $this->db->where('room_category_key', $room_category_key);
        $query = $this->db->get();

        $row = $query->row_array();

        return $row;
    }

    public function get_available_rooms_by_category($room_category_key)
    {
        $room_count = (int)0;

        $this->db->select('*');
        $this->db->from($this->room_category_table);
        $this->db->where('room_category_key', $room_category_key);
        $query = $this->db->get();

        $row = $query->row_array();

        if(!empty($row)){
            $room_count = $row['total_number_of_rooms_in_category'] - $row['booked_rooms_in_category'] ;
        }

        return (int)$room_count;
    }

    public function get_room_category_with_images_by_key($room_category_key)
    {
        $room_category = $this->get_room_category_by_key($room_category_key);

        if (!empty($room_category)) {
            $room_category = $this->put_related_items_in_room_category($room_category);
        }

        return $room_category;
    }

    public function get_room_category_id_by_key($room_category_key)
    {
        $id = 0;
        $room_category = $this->get_room_category_by_key($room_category_key);

        if (!empty($room_category)) {
            $id = $room_category['room_category_id'];
        }

        return $id;
    }

    public function update_room_category($room_category, $room_category_images, $room_category_key)
    {
        $room_category['room_category_updated_at'] = date("Y-m-d H:i:s");

        $this->db->update($this->room_category_table, $room_category, array('room_category_key' => $room_category_key));

        $room_category_id = $this->get_room_category_id_by_key($room_category_key);

        if (isset($room_category_images) && $room_category_id > 0) {
            if (!empty($room_category_images)) {
                foreach ($room_category_images as $image) {
                    $room_category_image = array();
                    $room_category_image['room_category_id'] = $room_category_id;
                    $room_category_image['room_category_image_name'] = $image;
                    $this->db->insert($this->room_category_image_table, $room_category_image);
                }
            }

        }

        return $room_category_key;

    }

    public function room_category_image_remove($room_category_image_id)
    {
        $image_with_path = $this->get_room_category_image_with_path($room_category_image_id, $file_path = true);

        if ($image_with_path != "") {
            if (file_exists($image_with_path)) {
                unlink($image_with_path);
            }
        }

        $this->db->delete($this->room_category_image_table, array('room_category_image_id' => $room_category_image_id));

    }

}