<?php

class Template_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

    }

    public function render($file, $data)
    {
        $view = $this->load->view($file, $data, true);

        return $view;
    }

    public function auth_master($data)
    {
        return $this->render('auth_master', $data, true);
    }

    public function master($data)
    {
        return $this->render('master', $data, true);
    }

    public function blank_error_master($data)
    {
        return $this->render('blank_error_master', $data, true);
    }

    public function headlink($params)
    {
        $data = array();
        return $this->render('segments/headlink', $data, true);
    }

    public function meta($params)
    {
        $data = array();
        return $this->render('segments/meta', $data, true);
    }

    public function navbar($params)
    {
        $data = array();

        $user_id = $this->session->userdata('user_id') ? $this->session->userdata('user_id') : 0;
        $data['user'] = $this->user->get_user_by_id($user_id);
        return $this->render('segments/navbar', $data, true);
    }

    public function sidebar($params)
    {
        $data = array();
        return $this->render('segments/sidebar', $data, true);
    }

    public function footer($params)
    {
        $data = array();
        return $this->render('segments/footer', $data, true);
    }

    public function footlink($params)
    {
        $data = array();
        return $this->render('segments/footlink', $data, true);
    }

}