<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CustomerController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Customer_model', 'customer');

    }

    public function customer_list()
    {
        $this->user->admin_permitted_or_thrown_out();

        $data = array();

        $data['title'] = "Customer List";

        $data['meta'] = $this->template->meta(array());
        $data['headlink'] = $this->template->headlink(array());
        $data['navbar'] = $this->template->navbar(array());
        $data['sidebar'] = $this->template->sidebar(array());
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->footlink(array());
        $data['content'] = $this->customer_list_and_forms_content($content_params = array());

        $view = $this->template->master($data);
        echo $view;
        exit;
    }

    public function customer_list_and_forms_content($content_params)
    {
        $data = $content_params;

        $content = $this->template->render('contents/admin/customer_page', $data, true);

        return $content;
    }

    public function ajax_list()
    {
        $list = $this->customer->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $customer) {
            $no++;
            $row = array();
            $row[] = $customer['user_name'];
            $row[] = $customer['user_email'];
            $row[] = $customer['user_active'] == 1 ? "Active" : "Inactive";
            $row[] = $customer['user_created_at'];
            $row[] = $customer['user_updated_at'];

            //add html for action
            $customer['action'] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_customer(' . "'" . $customer['user_id'] . "'" . ')"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_customer(' . "'" . $customer['user_id'] . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a>';
            $row[] = $customer['action'];
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->customer->count_all(),
            "recordsFiltered" => $this->customer->count_filtered(),
            "data" => $data,
        );
        //output to json format

        echo json_encode($output);
        die();
    }

    public function ajax_edit($id)
    {
        $data = $this->user->get_user_by_id($id);

        echo json_encode($data);
        die();
    }

    public function ajax_add()
    {
        $this->_validate($change_password = true, $with_email = true);
        $this->_validate($change_password, $with_email = true);

        $data = array(
            'user_name' => trim($this->input->post('user_name')),
            'user_email' => trim($this->input->post('user_email')), //must check email not duplicate
            'user_active' => $this->input->post('user_active'),
        );

        if ($change_password) {
            $data['user_password'] = $this->auth->password_hash(trim($this->input->post('user_password')));
        }

        $insert = $this->user->create_user($data, 'customer');
        echo json_encode(array("status" => TRUE));
        die();
    }

    public function ajax_update()
    {
        $change_password = false;

        if (trim($this->input->post('user_password')) != '') {
            $change_password = true;
        }

        $this->_validate($change_password, $with_email = false);
        $data = array(
            'user_name' => trim($this->input->post('user_name')),
            'user_active' => $this->input->post('user_active'),
            'user_updated_at' => date('Y-m-d H:i:s'),
        );

        if ($change_password) {
            $data['user_password'] = $this->auth->password_hash(trim($this->input->post('user_password')));
        }

        $this->customer->update(array('user_id' => $this->input->post('user_id')), $data);
        echo json_encode(array("status" => TRUE));
        die();
    }

    public function ajax_delete($id)
    {
        //$this->customer->delete_by_id($id);
        $data['user_deleted'] = 1;
        $this->customer->update(array('user_id' => $this->input->post('user_id')), $data);
        echo json_encode(array("status" => TRUE));
        die();
    }

    private function _email_validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        $posted_email = $this->input->post('user_email');

        if ($posted_email == '') {
            $data['inputerror'][] = 'user_email';
            $data['error_string'][] = 'Email is required';
            $data['status'] = FALSE;
        }

        if ($posted_email != '') {
            if (!filter_var($posted_email, FILTER_VALIDATE_EMAIL)) {
                $data['inputerror'][] = 'user_email';
                $data['error_string'][] = "{$posted_email} is invalid";
                $data['status'] = FALSE;
            }

            if ($this->user->if_user_email_exists($posted_email)) {
                $data['inputerror'][] = 'user_email';
                $data['error_string'][] = "{$posted_email} is already exists";
                $data['status'] = FALSE;
            }
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }


    }


    private function _validate($change_password, $with_email)
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('user_name') == '') {
            $data['inputerror'][] = 'user_name';
            $data['error_string'][] = 'First name is required';
            $data['status'] = FALSE;
        }

        if ($this->input->post('user_active') == '') {
            $data['inputerror'][] = 'user_active';
            $data['error_string'][] = 'Please select user status';
            $data['status'] = FALSE;
        }

        if ($change_password) {

            if ($this->input->post('user_password') == '') {
                $data['inputerror'][] = 'user_password';
                $data['error_string'][] = 'Password is required';
                $data['status'] = FALSE;
            }

            if ($this->input->post('confirm_password') == '') {
                $data['inputerror'][] = 'confirm_password';
                $data['error_string'][] = 'Confirm password is required';
                $data['status'] = FALSE;
            }

            if ($this->input->post('user_password') != $this->input->post('confirm_password')) {
                $data['inputerror'][] = 'confirm_password';
                $data['error_string'][] = 'Password and Confirm Password does not match';
                $data['status'] = FALSE;
            }
        }

        if ($with_email) {
            $posted_email = $this->input->post('user_email');

            if ($posted_email == '') {
                $data['inputerror'][] = 'user_email';
                $data['error_string'][] = 'Email is required';
                $data['status'] = FALSE;
            }

            if ($posted_email != '') {
                if (!filter_var($posted_email, FILTER_VALIDATE_EMAIL)) {
                    $data['inputerror'][] = 'user_email';
                    $data['error_string'][] = "{$posted_email} is invalid";
                    $data['status'] = FALSE;
                }

                if ($this->user->if_user_email_exists($posted_email)) {
                    $data['inputerror'][] = 'user_email';
                    $data['error_string'][] = "{$posted_email} is invalid";
                    $data['status'] = FALSE;
                }
            }
        }


        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }


}
