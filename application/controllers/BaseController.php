<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BaseController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $redirect = $this->user->is_admin() ? "admin/dashboard" : "dashboard";
        return redirect($redirect);
    }

    public function permission_denied()
    {
        $data = array();

        $data['title'] = "Permission Denied";

        $data['meta'] = $this->template->meta(array());
        $data['headlink'] = $this->template->headlink(array());
        $data['footlink'] = $this->template->footlink(array());

        $content_params = array();
        $content_params['blank_page_h1'] = "Permission Denied";
        $content_params['blank_page_top_p'] = "You are not allowed in the requested page";
        $content_params['blank_page_bottom_p'] = "Please go back";
        $data['content'] = $this->blank_error_content($content_params);

        $view = $this->template->blank_error_master($data);
        echo $view;
        exit;
    }

    public function blank_error_content($content_params)
    {
        $data = $content_params;

        $content = $this->template->render('contents/blank_error_page', $data, true);

        return $content;
    }

    public function page_not_found()
    {
        $data = array();

        $data['title'] = "Page not found";

        $data['meta'] = $this->template->meta(array());
        $data['headlink'] = $this->template->headlink(array());
        $data['footlink'] = $this->template->footlink(array());

        $content_params = array();
        $content_params['blank_page_h1'] = "Page not found";
        $content_params['blank_page_top_p'] = "Requested page is not found";
        $content_params['blank_page_bottom_p'] = "Please go back";
        $data['content'] = $this->blank_error_content($content_params);

        $view = $this->template->blank_error_master($data);
        echo $view;
        exit;
    }

    public function invalid_action()
    {
        $data = array();

        $data['title'] = "Invalid action";

        $data['meta'] = $this->template->meta(array());
        $data['headlink'] = $this->template->headlink(array());
        $data['footlink'] = $this->template->footlink(array());

        $content_params = array();
        $content_params['blank_page_h1'] = "Invalid action";
        $content_params['blank_page_top_p'] = "Invalid action";
        $content_params['blank_page_bottom_p'] = "Please go back";
        $data['content'] = $this->blank_error_content($content_params);

        $view = $this->template->blank_error_master($data);
        echo $view;
        exit;
    }


}