<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DashboardController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        if($this->user->is_admin()){
            redirect('admin/dashboard');
        }
        $data = array();

        $data['title'] = "User - Dashboard";

        $data['meta'] = $this->template->meta(array());
        $data['headlink'] = $this->template->headlink(array());
        $data['navbar'] = $this->template->navbar(array());
        $data['sidebar'] = $this->template->sidebar(array());
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->footlink(array());
        $data['content'] = $this->dashboard_content($content_params = array());

        $view = $this->template->master($data);
        echo $view;
        exit;
    }


    public function dashboard_content($content_params)
    {
        $data = array();

        $content =  $this->template->render('contents/common/dashboard_page',$data, true);

        return $content;
    }


}
