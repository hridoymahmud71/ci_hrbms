<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AuthController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function admin_login()
    {
        if ($this->user->is_logged_in() && $this->user->is_admin()) {
            redirect('admin/dashboard');
        }

        if ($this->user->is_customer()) {
            $this->customer_logout();
        }


        if ($this->input->post()) {
            $this->admin_login_post();
            exit;
        }

        $data = array();
        $data['title'] = "Admin Login";
        $data['meta'] = $this->template->meta(array());
        $data['headlink'] = $this->template->headlink(array());
        $data['footlink'] = $this->template->footlink(array());

        $content_params = array();
        $content_params['action'] = "admin/login";
        $content_params['forgot_password_url'] = "admin/forgot-password";
        $content_params['panel'] = "Admin";

        $data['content'] = $this->login_content($content_params);

        $view = $this->template->auth_master($data);
        echo $view;
        exit;
    }

    public function login_content($content_params)
    {
        $data = $content_params;

        $content = $this->template->render('contents/auth/login_page', $data, true);

        return $content;
    }

    public function admin_login_post()
    {
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('admin/login');
        }

        $posted_email = trim($this->input->post('email'));
        $posted_password = $this->input->post('password');
        $this->session->set_flashdata('flash_email', $posted_email);

        $user = $this->user->get_user_by_email($posted_email);

        if (empty($user)) {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('email_not_found', "Given email is not found");
            redirect('admin/login');
        }

        if (empty($user)) {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('user_inactive', "User is inactive ");
            redirect('admin/login');
        }

        if (!$this->auth->password_verify($posted_password, $user['user_password'])) {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('password_mismatch', "Given password does not match");
            redirect('admin/login');
        }

        $this->auth->set_user_id_in_session($user['user_id']);
        redirect('admin/dashboard');
    }

    //---------------------------------------------

    public function customer_login()
    {
        if ($this->user->is_logged_in() && $this->user->is_customer()) {
            redirect('dashboard');
        }

        if ($this->user->is_admin()) {
            $this->admin_logout();
        }

        if ($this->input->post()) {
            $this->customer_login_post();
            exit;
        }

        $data = array();
        $data['title'] = "Customer Login";
        $data['meta'] = $this->template->meta(array());
        $data['headlink'] = $this->template->headlink(array());
        $data['footlink'] = $this->template->footlink(array());

        $content_params = array();
        $content_params['action'] = "login";
        $content_params['forgot_password_url'] = "forgot-password";
        $content_params['panel'] = "Customer";

        $data['content'] = $this->login_content($content_params);

        $view = $this->template->auth_master($data);
        echo $view;
        exit;


    }

    public function customer_login_post()
    {
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');


        if ($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('login');
        }


        $posted_email = trim($this->input->post('email'));
        $posted_password = $this->input->post('password');
        $this->session->set_flashdata('flash_email', $posted_email);

        $user = $this->user->get_user_by_email($posted_email);

        if (empty($user)) {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('email_not_found', "Given email is not found");
            redirect('login');
        }


        if ($user['user_active'] != 1) {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('user_inactive', "User is inactive ");
            redirect('login');
        }

        if (!$this->auth->password_verify($posted_password, $user['user_password'])) {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('password_mismatch', "Given password does not match");
            redirect('login');
        }

        $this->auth->set_user_id_in_session($user['user_id']);
        redirect('dashboard');
    }

    //-------------------------------------------------------------------
    public function customer_regisration()
    {
        if ($this->user->is_logged_in() && $this->user->is_customer()) {
            $this->customer_logout();
        }

        if ($this->user->is_admin()) {
            $this->admin_logout();
        }

        if ($this->input->post()) {
            $this->customer_registration_post();
            exit;
        }

        $data = array();
        $data['title'] = "Customer registration";
        $data['meta'] = $this->template->meta(array());
        $data['headlink'] = $this->template->headlink(array());
        $data['footlink'] = $this->template->footlink(array());

        $content_params = array();
        $content_params['action'] = "registration";
        $content_params['forgot_password_url'] = "forgot-password";
        $content_params['login_url'] = "login";
        $content_params['panel'] = "Customer";

        $data['content'] = $this->registration_content($content_params);

        $view = $this->template->auth_master($data);
        echo $view;
        exit;


    }

    public function registration_content($content_params)
    {
        $data = $content_params;

        $content = $this->template->render('contents/auth/registration_page', $data, true);

        return $content;
    }

    public function customer_registration_post()
    {
        $this->form_validation->set_rules('name', 'Name', 'required|trim|min_length[2]|max_length[50]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim|is_unique[user.user_email]');
        $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[8]');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('registration');
        }

        $posted_name = trim($this->input->post('name'));
        $posted_email = trim($this->input->post('email'));
        $posted_password = $this->input->post('password');
        $posted_re_password = $this->input->post('re_password');

        if ($posted_password != $posted_re_password) {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('password_mismatch', "Passwords do not match");
            redirect('registration');
        }

        $ins_data['user_name'] = $posted_name;
        $ins_data['user_email'] = $posted_email;
        $ins_data['user_password'] = $posted_password;

        $ret = $this->user->create_user($ins_data, 'customer');

        if (isset($ret['user_id'])) {
            if (is_numeric($ret['user_id']) && $ret['user_id'] > 0) {
                $activation_code = $this->auth->generate_and_insert_activation_code($ret['user_id']);

                $url = BASE_URL . "apply-activation-code?activation_code={$activation_code}";
                $message = "Hello {$ins_data['user_name']}, <a href='{$url}'>Click Here To Verify Your Account</a>  ";

                $subject = "Hotel Room Booking Management System Activation Code";

                $mail_data['to'] = $posted_email;
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->custom_email->sendEmail($mail_data);

                $this->session->set_flashdata('success', 'success');
                $this->session->set_flashdata('activation_code_sent', "Activation code is sent via email,please check");
            }
        }

        $this->session->set_flashdata('flash_name', $posted_name);
        $this->session->set_flashdata('flash_email', $posted_email);


        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('registration_success', "Registration Complete");
        redirect('registration');
    }

    public function apply_activation_code()
    {
        if (isset($_REQUEST['activation_code'])) {
            if (!empty($_REQUEST['activation_code'])) {
                $this->auth->process_activation_code($_REQUEST['activation_code']);

                $this->session->set_flashdata('success', 'success');
                $this->session->set_flashdata('activation_applied', "Activation applied,you can now log in");
                redirect('login');
            }

        }
    }

    //----------------------------------


    public function admin_logout()
    {
        $this->auth->sess_destroy();
        redirect('admin/login');
    }

    public function customer_logout()
    {
        $this->auth->sess_destroy();
        redirect('login');
    }


}