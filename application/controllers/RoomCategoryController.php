<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RoomCategoryController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Room_category_model", "room_category");
    }

    public function room_category($room_category_key)
    {
        $data = array();

        $data['title'] = "Room Categorie";

        $data['meta'] = $this->template->meta(array());
        $data['headlink'] = $this->template->headlink(array());
        $data['navbar'] = $this->template->navbar(array());
        $data['sidebar'] = $this->template->sidebar(array());
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->footlink(array());


        $content_params = array();
        $content_params['room_category_key'] = $room_category_key;
        $data['content'] = $this->room_category_content($content_params);

        $view = $this->template->master($data);
        echo $view;
        exit;
    }

    public function room_category_content($content_params)
    {
        $data = $content_params;

        $data['room_category'] = $this->room_category->get_room_category_with_images_by_key($content_params['room_category_key']);

        //echo "<pre>";print_r($data['room_category']);exit;
        if (empty($data['room_category'])) {
            redirect('page-not-found');
        }

        $content = $this->template->render('contents/common/room_category/room_category_view_page', $data, true);

        return $content;
    }

    public function room_category_list()
    {
        $data = array();

        $data['title'] = "Room Categorie";

        $data['meta'] = $this->template->meta(array());
        $data['headlink'] = $this->template->headlink(array());
        $data['navbar'] = $this->template->navbar(array());
        $data['sidebar'] = $this->template->sidebar(array());
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->footlink(array());


        $content_params = array();

        $data['content'] = $this->room_category_list_content($content_params);

        $view = $this->template->master($data);
        echo $view;
        exit;
    }

    public function room_category_list_content($content_params)
    {
        $data = $content_params;

        $data['room_categories'] = $this->room_category->get_room_categories($limit = false);
        //echo "<pre>";print_r($data['room_categories']);exit;

        $content = $this->template->render('contents/common/room_category/room_category_list_page', $data, true);

        return $content;
    }

    //------------------------------------------------------------------------------------------------------------------

    public function room_category_add()
    {
        $this->user->admin_permitted_or_thrown_out();

        if ($this->input->post()) {
            $this->room_category_add_post();
            exit;
        }

        $data = array();

        $data['title'] = "Add A Room Category";

        $data['meta'] = $this->template->meta(array());
        $data['headlink'] = $this->template->headlink(array());
        $data['navbar'] = $this->template->navbar(array());
        $data['sidebar'] = $this->template->sidebar(array());
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->footlink(array());


        $content_params = array();
        $content_params['form_title'] = "Add A Room Category";
        $content_params['which_form'] = "add";
        $content_params['form_action'] = "room-category-add";
        $content_params['room_key'] = "";


        $data['content'] = $this->room_category_form_content($content_params);

        $view = $this->template->master($data);
        echo $view;
        exit;
    }

    public function room_category_edit($room_category_key)
    {
        $this->user->admin_permitted_or_thrown_out();

        if ($this->input->post()) {
            $this->room_category_edit_post();
            exit;
        }

        $data = array();

        $data['title'] = "Edit Room Category";

        $data['meta'] = $this->template->meta(array());
        $data['headlink'] = $this->template->headlink(array());
        $data['navbar'] = $this->template->navbar(array());
        $data['sidebar'] = $this->template->sidebar(array());
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->footlink(array());


        $content_params = array();
        $content_params['form_title'] = "Edit Room Category";
        $content_params['which_form'] = "edit";
        $content_params['form_action'] = "room-category-edit/{$room_category_key}";
        $content_params['room_key'] = $room_category_key;


        $data['content'] = $this->room_category_form_content($content_params);

        $view = $this->template->master($data);
        echo $view;
        exit;
    }


    public function room_category_form_content($content_params)
    {
        $data = $content_params;

        $data['room_category_key'] = "";
        $data['room_category_name'] = "";
        $data['room_category_description'] = "";
        $data['room_category_price'] = "";
        $data['total_number_of_rooms_in_category'] = "";

        $room_category = array();
        if ($content_params['room_key'] != "") {
            $room_category = $this->room_category->get_room_category_with_images_by_key($content_params['room_key']);
        }

        if (!empty($room_category)) {
            $data['room_category_key'] = $room_category['room_category_key'];
            $data['room_category_name'] = $room_category['room_category_name'];
            $data['room_category_description'] = $room_category['room_category_description'];
            $data['room_category_price'] = $room_category['room_category_price'];
            $data['total_number_of_rooms_in_category'] = $room_category['total_number_of_rooms_in_category'];
            $data['room_category_images'] = $room_category['room_category_images'];
        } else if ($this->session->flashdata()) {
            $data['room_category_name'] = $this->session->flashdata('room_category_name') ? $this->session->flashdata('room_category_name') : "";
            $data['room_category_price'] = $this->session->flashdata('room_category_price') ? $this->session->flashdata('room_category_price') : "";
            $data['total_number_of_rooms_in_category'] = $this->session->flashdata('total_number_of_rooms_in_category') ? $this->session->flashdata('total_number_of_rooms_in_category') : "";
        }

        $content = $this->template->render('contents/common/room_category/room_category_form_page', $data, true);

        return $content;
    }

    public function room_category_add_post()
    {
        $this->form_validation->set_rules('room_category_name', 'Room Category Name', 'required|trim|is_unique[room_category.room_category_name]');
        $this->form_validation->set_rules('room_category_price', 'Room Category Price', 'required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('total_number_of_rooms_in_category', 'Available Rooms', 'required|greater_than_equal_to[0]');

        $room_category['room_category_name'] = trim($this->input->post('room_category_name'));
        $room_category['room_category_description'] = $this->input->post('room_category_description');
        $room_category['room_category_price'] = trim($this->input->post('room_category_price'));
        $room_category['total_number_of_rooms_in_category'] = trim($this->input->post('total_number_of_rooms_in_category'));
        $room_category_images = $this->input->post('room_category_images');


        if ($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('room_category_name', $room_category['room_category_name']);
            $this->session->set_flashdata('room_category_price', $room_category['room_category_price']);
            $this->session->set_flashdata('total_number_of_rooms_in_category', $room_category['total_number_of_rooms_in_category']);

            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('room-category-add');
        }

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('add_success', "Successfully Added ");
        $room_category_key = $this->room_category->create_room_category($room_category, $room_category_images);
        redirect('room-category-edit/' . $room_category_key);

    }

    public function room_category_edit_post()
    {
        $this->form_validation->set_rules('room_category_name', 'Room Category Name', 'required|trim');
        $this->form_validation->set_rules('room_category_price', 'Room Category Price', 'required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('total_number_of_rooms_in_category', 'Available Rooms', 'required|greater_than_equal_to[0]');

        $room_category_key = $this->input->post('room_category_key');
        $room_category['room_category_name'] = trim($this->input->post('room_category_name'));
        $room_category['room_category_description'] = $this->input->post('room_category_description');
        $room_category['room_category_price'] = trim($this->input->post('room_category_price'));
        $room_category['total_number_of_rooms_in_category'] = trim($this->input->post('total_number_of_rooms_in_category'));
        $room_category_images = $this->input->post('room_category_images');


        if ($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('room_category_name', $room_category['room_category_name']);
            $this->session->set_flashdata('room_category_price', $room_category['room_category_price']);
            $this->session->set_flashdata('total_number_of_rooms_in_category', $room_category['total_number_of_rooms_in_category']);

            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('room-category-edit');
        }

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('update_success', "Successfully Updated ");
        $room_category_key = $this->room_category->update_room_category($room_category, $room_category_images, $room_category_key);
        redirect('room-category-edit/' . $room_category_key);

    }

    public function room_category_image_upload()
    {
        $files = $_FILES;
        if (isset($_FILES['image']) && !empty($_FILES['image']['name'])) {
            $config['encrypt_name'] = true;
            $config['upload_path'] = $this->config->item('room_category_image_upload_path');
            $config['allowed_types'] = 'jpg|png|gif|jpeg';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('image')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }

    public function room_category_delete($room_category_key)
    {
        $room_category['room_category_deleted'] = 1;
        $room_category_key = $this->room_category->update_room_category($room_category, $room_category_images = array(), $room_category_key);
        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('delete_success', "Successfully Deleted ");

        redirect('room-category-list');
    }

    public function room_category_image_remove()
    {
        $room_category_image_id = $this->input->post('room_category_image_id');


        $this->room_category->room_category_image_remove($room_category_image_id);
        echo "done";
        exit;
    }


}
