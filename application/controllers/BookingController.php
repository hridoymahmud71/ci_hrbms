<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BookingController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Room_category_model", "room_category");
        $this->load->model("Booking_model", "booking");

        $this->load->library('paypal_lib');
    }

    public function book()
    {
        $this->user->customer_permitted_or_thrown_out();

        $data = array();

        $data['title'] = "Book Rooms";

        $data['meta'] = $this->template->meta(array());
        $data['headlink'] = $this->template->headlink(array());
        $data['navbar'] = $this->template->navbar(array());
        $data['sidebar'] = $this->template->sidebar(array());
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->footlink(array());

        $content_params = array();
        $content_params['pre_selected_room_category'] = isset($_REQUEST['pre_selected_room_category']) ? $_REQUEST['pre_selected_room_category'] : "";

        $data['content'] = $this->book_content($content_params);


        $view = $this->template->master($data);
        echo $view;
        exit;
    }


    public function book_content($content_params)
    {
        $data = $content_params;

        $data['room_categories'] = $this->room_category->get_room_categories($limit = 500);

        $content = $this->template->render('contents/customer/book_page', $data, true);

        return $content;
    }

    public function order_request()
    {
        $json_str = file_get_contents('php://input');
        # Get as an object
        $request = json_decode($json_str, 1);

        /*echo "<pre>";
        print_r($request);
        exit;*/

        $response = array();
        $response['original_request'] = $request;
        $response['success'] = true;
        $response['message'] = "";
        $response['redirect'] = "";

        if (!$this->user->is_logged_in()) {
            $response['success'] = false;
            $response['message'] = "Need to login";
            echo json_encode($response);
            exit;
        }
        if (!$this->user->is_customer()) {
            $response['success'] = false;
            $response['message'] = "Need to login as customer";
            echo json_encode($response);
            exit;
        }

        if (empty($request)) {
            $response['success'] = false;
            $response['message'] = "Nothing to process";
            echo json_encode($response);
            exit;
        }

        if (!empty($request)) {
            $availability_message = array();
            $availability_err = 0;
            $booking_order = array();
            $booking_order['booking_order_amount'] = 0.00;
            $booking_order['user_id'] = $this->session->userdata('user_id');
            $booking_order_booking_category = array();

            $booking_order_booking_categories = array();
            foreach ($request as $item) {
                $room_category = $this->room_category->get_room_category_by_key($item['key']);
                $available = 0;

                $booking_order['booking_order_amount'] += $item['count'] * $room_category['room_category_price'];


                if (!empty($room_category)) {
                    $available = $room_category['total_number_of_rooms_in_category'] - $room_category['booked_rooms_in_category'];
                    if ($item['count'] > $available) {
                        $availability_err++;
                        $availability_message[] = "<p>{$item['name']} has {$available} room(s) available</p>";
                    }

                    $booking_order_booking_category = array();
                    $booking_order_booking_category['room_category_id'] = $room_category['room_category_id'];
                    $booking_order_booking_category['room_count'] = $item['count'];
                    $booking_order_booking_categories[] = $booking_order_booking_category;


                }

            }

            if ($availability_err > 0) {
                $response['success'] = false;
                $response['message'] = implode("", $availability_message);
                echo json_encode($response);
                exit;
            }

            if ($booking_order['booking_order_amount'] == 0.00) {
                $response['success'] = false;
                $response['message'] = "Booking order amount cannot be {$booking_order['booking_order_amount']}";
                echo json_encode($response);
                exit;
            }


            //all pass...........

            $booking_order_key = $this->booking->create_booking_order($booking_order, $booking_order_booking_categories);

            if (!empty($booking_order_key)) {
                $response['success'] = true;
                $response['message'] = "order inserted";
                $response['redirect'] = "booking/paypal-form/{$booking_order_key}";
                echo json_encode($response);
                exit;
            }
            //----------------------------------------------------------------------------------------------------------


        }

        echo json_encode($response);
        exit;

    }

    public function paypal_form($booking_order_key)
    {
        $returnURL = base_url() . 'booking/paypal-success';
        $cancelURL = base_url() . 'booking/paypal-cancel';
        $notifyURL = base_url() . 'booking/paypal-ipn';

        $user = $this->user->get_user_by_id($this->session->userdata('user_id'));

        if (empty($user)) {
            redirect('page-not-found');
        }

        $booking_order = $this->booking->get_booking_order_by_key($booking_order_key);

        if (empty($booking_order)) {
            redirect('page-not-found');
        }

        // Add fields to paypal form
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);
        $this->paypal_lib->add_field('item_name', $booking_order_key . ' ' . $user['user_key'] . ' ' . $booking_order['booking_order_amount']);
        $this->paypal_lib->add_field('custom', $user['user_key']);
        $this->paypal_lib->add_field('item_number', $booking_order_key);
        $this->paypal_lib->add_field('amount', $booking_order['booking_order_amount']);

        // Render paypal form
        $this->paypal_lib->paypal_auto_form();
    }


    public function paypal_success()
    {
        $data = array();

        $data['title'] = "Paypal payment successful";

        $data['meta'] = $this->template->meta(array());
        $data['headlink'] = $this->template->headlink(array());
        $data['footlink'] = $this->template->footlink(array());

        $content_params = array();

        $content_params['item_name'] = "";
        $content_params['item_number'] = "";
        $content_params['txn_id'] = "";
        $content_params['payment_amt'] = "";
        $content_params['currency_code'] = "";
        $content_params['status'] = "";

        // Get the transaction data
        $paypalInfo = $_REQUEST;

        if (!empty($paypalInfo)) {
            $content_params['item_name'] = isset($paypalInfo['item_name']) ? $paypalInfo['item_name'] : "";
            $content_params['item_number'] = isset($paypalInfo['item_number']) ? $paypalInfo['item_number'] : "";
            $content_params['txn_id'] = isset($paypalInfo["txn_id"]) ? $paypalInfo["txn_id"] : "";
            $content_params['payment_amt'] = isset($paypalInfo["payment_gross"]) ? $paypalInfo["payment_gross"] : "";
            $content_params['currency_code'] = isset($paypalInfo["mc_currency"]) ? $paypalInfo["mc_currency"] : "";
            $content_params['status'] = isset($paypalInfo["payment_status"]) ? $paypalInfo["payment_status"] : "";

            $ignore_ipn = true;
            if ($ignore_ipn) {
                // Insert the transaction data in the database
                $paypal_transaction['user_key'] = $paypalInfo["custom"];
                $paypal_transaction['booking_order_key'] = $paypalInfo["item_number"];
                $paypal_transaction['txn_id'] = $paypalInfo["txn_id"];
                $paypal_transaction['payment_gross'] = $paypalInfo["mc_gross"];
                $paypal_transaction['currency_code'] = $paypalInfo["mc_currency"];
                $paypal_transaction['payer_email'] = $paypalInfo["payer_email"];
                $paypal_transaction['payment_status'] = $paypalInfo["payment_status"];

                $this->booking->insert_paypal_transaction($paypal_transaction);

                $boooking_order = array();
                $boooking_order['booking_order_is_paid'] = 1;
                $boooking_order['booking_order_paid_at'] = date('Y-m-d H:i:s');
                $this->booking->update_booking_order($boooking_order, array(), $paypal_transaction['booking_order_key']);

                $this->notify_ipn_to_admins($paypal_transaction);
            }

        }


        $data['content'] = $this->paypal_success_content($content_params);

        $view = $this->template->master($data);
        echo $view;
        exit;
    }

    public function paypal_success_content($content_params)
    {
        $data = $content_params;

        $content = $this->template->render('contents/paypal/success', $data, true);

        return $content;
    }

    public function paypal_cancel()
    {
        $data = array();

        $data['title'] = "Paypal payment failed";

        $data['meta'] = $this->template->meta(array());
        $data['headlink'] = $this->template->headlink(array());
        $data['footlink'] = $this->template->footlink(array());

        $content_params = array();

        $data['content'] = $this->paypal_cancel_content($content_params);

        $view = $this->template->master($data);
        echo $view;
        exit;
    }

    public function paypal_cancel_content($content_params)
    {
        $data = $content_params;

        $content = $this->template->render('contents/paypal/cancel', $data, true);

        return $content;
    }

    function paypal_ipn()
    {
        // Paypal posts the transaction data
        $paypalInfo = $_REQUEST;

        if (!empty($paypalInfo)) {
            // Validate and get the ipn response
            $ipnCheck = $this->paypal_lib->validate_ipn($paypalInfo);

            // Check whether the transaction is valid
            if ($ipnCheck) {
                // Insert the transaction data in the database
                $paypal_transaction['user_key'] = $paypalInfo["custom"];
                $paypal_transaction['booking_order_key'] = $paypalInfo["item_number"];
                $paypal_transaction['txn_id'] = $paypalInfo["txn_id"];
                $paypal_transaction['payment_gross'] = $paypalInfo["mc_gross"];
                $paypal_transaction['currency_code'] = $paypalInfo["mc_currency"];
                $paypal_transaction['payer_email'] = $paypalInfo["payer_email"];
                $paypal_transaction['payment_status'] = $paypalInfo["payment_status"];

                $this->booking->insert_paypal_transaction($paypal_transaction);

                $boooking_order = array();
                $boooking_order['booking_order_is_paid'] = 1;
                $boooking_order['booking_order_paid_at'] = date('Y-m-d H:i:s');
                $this->booking->update_booking_order($boooking_order, array(), $paypal_transaction['booking_order_key']);

                $this->notify_ipn_to_admins($paypal_transaction);
            }
        }
    }

    public function notify_ipn_to_admins($paypal_transaction)
    {
        $admins = $this->user->get_users_by_role('admin');

        if (!empty($admins)) {
            foreach ($admins as $admin) {
                $this->notify_ipn_to_admin($admin, $paypal_transaction);
            }
        }
    }

    public function notify_ipn_to_admin($admin, $paypal_transaction)
    {

        $subject = "A paypal transaction happended for a booking";
        $message = '';
        $message .= "Hi {$admin['user_name']} ,<br>
                         A payal transaction happened for a specific booking<br>
                         Details below:<br>
                         --------------------------------- <br>
                         <b>User key:</b> {$paypal_transaction['user_key']} <br>
                         <b>Booking order key:</b> {$paypal_transaction['booking_order_key']} <br>                                      
                         <b>Transaction_id:</b> {$paypal_transaction['txn_id']} <br>      
                         <b>Gross Payment:</b>{$paypal_transaction['currency_code']} {$paypal_transaction['payment_gross']}  <br>    
                         <b>Payer email:</b> {$paypal_transaction['payer_email']}<br>                                    
                         <b>Payment status:</b> {$paypal_transaction['payment_status']}<br>                                    
                         --------------------------------- <br><br>
                         Thank you <br><br>
                        ";


        $mail_data['to'] = $admin['user_email'];
        $mail_data['subject'] = $subject;
        $mail_data['message'] = $message;

        $mail_sent = $this->custom_email->sendEmail($mail_data);

    }

    //------------------------------------------------------------------------------------------------------------------

    public function booking_list()
    {
        if (!$this->user->is_logged_in()) {
            redirect('login');
        }

        $data = array();

        $data['title'] = $this->user->is_customer() ? "My Bookings" : "All Bookings";

        $data['meta'] = $this->template->meta(array());
        $data['headlink'] = $this->template->headlink(array());
        $data['navbar'] = $this->template->navbar(array());
        $data['sidebar'] = $this->template->sidebar(array());
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->footlink(array());


        $data['content'] = $this->booking_list_content($content_params = array('title' => $data['title']));

        $view = $this->template->master($data);
        echo $view;
        exit;
    }

    public function booking_list_content($content_params)
    {
        $data = $content_params;

        $content = $this->template->render('contents/common/booking/booking_list_page', $data, true);

        return $content;
    }

    public function ajax_list()
    {
        $customer_id = $this->user->is_customer() ? $this->session->userdata('user_id') : 0;

        $list = $this->booking->get_datatables($customer_id);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $booking) {
            $no++;
            $row = array();
            $row[] = $booking['booking_order_key'];
            $row[] = $booking['user_name'];
            $row[] = $booking['user_email'];
            $row[] = $booking['payment_gross'];
            $row[] = $booking['txn_id'];
            $row[] = $booking['booking_order_approval'] == 1 ? "Approved" : "Waiting";
            $row[] = $booking['booking_order_updated_at'];


            //add html for action
            $booking['action'] = "";
            $booking['action'] .= "<div class='btn-group-vertical'>";
            $booking['action'] .= "<a href='booking-order/{$booking['booking_order_key']}' class='btn btn-primary' >View<a>";

            if (1) {
                $booking['action'] .= "<a href='booking-order-invoice-pdf/{$booking['booking_order_key']}' class='btn btn-warning' >Invoice Pdf<a>";
            }
            if ($this->user->is_admin()) {
                $booking['action'] .= "<a href='booking-order-invoice-email/{$booking['booking_order_key']}' class='btn btn-danger' >Email Invoice<a>";
            }
            $booking['action'] .= "</div>";
            $row[] = $booking['action'];
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->booking->count_all($customer_id),
            "recordsFiltered" => $this->booking->count_filtered($customer_id),
            "data" => $data,
        );
        //output to json format

        echo json_encode($output);
        die();
    }


    public function booking_order($booking_order_key)
    {
        if (!$this->user->is_logged_in()) {
            redirect('login');
        }

        $data = array();

        $data['title'] = "Booking order $booking_order_key";

        $data['meta'] = $this->template->meta(array());
        $data['headlink'] = $this->template->headlink(array());
        $data['navbar'] = $this->template->navbar(array());
        $data['sidebar'] = $this->template->sidebar(array());
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->footlink(array());

        $content_params = array();

        $content_params['title'] = $data['title'];
        $content_params['booking_order_key'] = $booking_order_key;

        $data['content'] = $this->booking_order_content($content_params);

        $view = $this->template->master($data);
        echo $view;
        exit;
    }

    public function booking_order_content($content_params)
    {
        $data = $content_params;

        $data['booking_order'] = $this->booking->get_detailed_booking_order_by_key($content_params['booking_order_key']);


        if (empty($data['booking_order'])) {
            redirect('page-not-found');
        }


        $content = $this->template->render('contents/common/booking/booking_order_view_page', $data, true);

        return $content;
    }

    public function booking_order_invoice_pdf($booking_order_key)
    {
        $html = $this->invoice_pdf_html($booking_order_key);
        $this->prepare_invoice_pdf($html, $mode = "show");
        exit;
    }

    public function booking_order_invoice_email($booking_order_key)
    {
        $this->user->admin_permitted_or_thrown_out();

        $this->send_booking_order_invoice_email($booking_order_key);

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('email_send_success', 'Email sent !');
        redirect('booking-list');
    }

    public function booking_order_approve($booking_order_key)
    {
        $this->user->admin_permitted_or_thrown_out();

        $booking_order = $this->booking->get_detailed_booking_order_by_key($booking_order_key);

        if (empty($booking_order)) {
            redirect('page-not-found');
        }

        if (!isset($booking_order['booking_order_room_categories'])) {
            die('rooms missing');
        }

        if ($booking_order['booking_order_approval'] == 1) {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('approve_error', "Already approved");
            redirect('booking-order/' . $booking_order_key);
        }

        $availability_err = 0;
        $availability_message = array();
        foreach ($booking_order['booking_order_room_categories'] as $booking_order_room_category) {

            if (!empty($booking_order_room_category)) {
                $available = $booking_order_room_category['total_number_of_rooms_in_category'] - $booking_order_room_category['booked_rooms_in_category'];


                if ($booking_order_room_category['room_count'] > $available) {
                    $availability_err++;
                    $availability_message[] = "<p>{$booking_order_room_category['room_category_name']} has {$available} room(s) available</p>";
                }

            }

        }

        if ($availability_err > 0) {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('approve_error', implode("", $availability_message));
            redirect('booking-order/' . $booking_order_key);
        }

        //-----------------------------------------------------------------------------------------
        foreach ($booking_order['booking_order_room_categories'] as $booking_order_room_category) {

            $room_category = array();
            $room_category['booked_rooms_in_category'] = $booking_order_room_category['booked_rooms_in_category'] + $booking_order_room_category['room_count'];
            $this->room_category->update_room_category($room_category, array(), $booking_order_room_category['room_category_key']);

        }

        $upd_booking_order = array();
        $upd_booking_order['booking_order_approval'] = 1;

        $this->booking->update_booking_order($upd_booking_order, array(), $booking_order_key);

        $this->send_booking_order_invoice_email($booking_order_key);

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('approve_success', "Order approved");
        $this->session->set_flashdata('email_send_success', "Invoice is sent");
        redirect('booking-order/' . $booking_order_key);
    }

    public function send_booking_order_invoice_email($booking_order_key)
    {
        $html = $this->invoice_pdf_html($booking_order_key);
        $pdf = $this->prepare_invoice_pdf($html, $mode = "email");
        $booking_order = $this->booking->get_detailed_booking_order_by_key($booking_order_key);

        if (!empty($booking_order)) {
            $message = "";
            $message .= "Hi {$booking_order['user_name']} ,<br>
                                        You have an invoice for a booking.Please check the file attached. Thank you<br>
                                        ";

            $subject = "You haave an invoice for booking {$booking_order_key}";

            $mail_data['to'] = $booking_order['user_email'];
            $mail_data['subject'] = $subject;
            $mail_data['message'] = $message;
            $mail_data['single_pdf_content'] = $pdf['single_pdf_content'];
            $mail_data['pdf_file_name'] = $pdf['pdf_file_name'];

            $this->custom_email->sendEmail($mail_data);
        }
    }

    public function invoice_pdf_html($booking_order_key)
    {
        $data = array();
        $data['booking_order'] = $this->booking->get_detailed_booking_order_by_key($booking_order_key);
        if (empty($data['booking_order'])) {
            redirect('page-not-found');
        }

        $view_data['meta'] = $this->template->meta(array());
        $view_data['headlink'] = $this->template->headlink(array());
        $view_data['content'] = $this->template->render('contents/common/booking/booking_order_invoice_pdf_page', $data, true);
        $view_data['footlink'] = $this->template->footlink(array());
        $html = $this->template->master($view_data);

        return $html;
    }


    private function prepare_invoice_pdf($html, $mode)
    {
        $this->load->library('MPDF/mpdf');

        $mpdf = new mPDF('win-1252', 'A4', '', '', 5, 5, 16, 60, 5, 5, 'C');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Invoice');
        $mpdf->SetAuthor($this->config->item('site_name'));
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;
        $filename = 'invoice' . time() . rand(10000, 99999) . '.pdf';
        $mpdf->WriteHTML($html);
        $content = '';

        if ($mode == 'email') {
            $mpdf->Output('uploads/email_attachments/' . $filename, 'F');
            $content = $mpdf->Output($filename, 'S');
        } else if ($mode == 'show') {
            $mpdf->Output($filename, 'I');
            exit;
        }


        return array('single_pdf_content' => $content, 'pdf_file_name' => $filename);
    }


}
