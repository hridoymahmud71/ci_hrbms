<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$route['default_controller'] = 'BaseController';
$route['404_override'] = 'BaseController/page_not_found';
$route['page-not-found'] = 'BaseController/page_not_found';
$route['translate_uri_dashes'] = FALSE;
$route['permission-denied'] = 'BaseController/permission_denied';
$route['invalid-action'] = 'BaseController/invalid_action';

//customer
$route['registration'] = 'AuthController/customer_regisration';
$route['apply-activation-code'] = 'AuthController/apply_activation_code';
$route['login'] = 'AuthController/customer_login';
$route['logout'] = 'AuthController/customer_logout';
$route['dashboard'] = 'DashboardController';

//room_category
$route['room-category-list'] = 'RoomCategoryController/room_category_list';
$route['room-category/(:any)'] = 'RoomCategoryController/room_category/$1';
$route['room-category-add'] = 'RoomCategoryController/room_category_add';
$route['room-category-edit/(:any)'] = 'RoomCategoryController/room_category_edit/$1';
$route['room-category-delete/(:any)'] = 'RoomCategoryController/room_category_delete/$1';
$route['room-category-image-upload'] = 'RoomCategoryController/room_category_image_upload';
$route['room-category-image-remove'] = 'RoomCategoryController/room_category_image_remove/';

//booking
$route['book'] = 'BookingController/book';
$route['order-request'] = 'BookingController/order_request';
$route['booking-list'] = 'BookingController/booking_list';
$route['booking/ajax-list'] = 'BookingController/ajax_list';

$route['booking-order/(:any)'] = 'BookingController/booking_order/$1';
$route['booking-order-approve/(:any)'] = 'BookingController/booking_order_approve/$1';
$route['booking-order-invoice-pdf/(:any)'] = 'BookingController/booking_order_invoice_pdf/$1';
$route['booking-order-invoice-email/(:any)'] = 'BookingController/booking_order_invoice_email/$1';

$route['booking/paypal-form/(:any)'] = "BookingController/paypal_form/$1";
$route['booking/paypal-success'] = "BookingController/paypal_success";
$coute['booking/paypal-cancel'] = "BookingController/paypal_cancel";
$noute['booking/paypal-ipn'] = "BookingController/paypal_ipn";

//room_booking_list
$route['room-booking-list'] = 'RoomBookingController/room_booking_list';
$route['room-booking/(:any)'] = 'RoomBookingController/room_booking/$1';
$route['room-booking-approve/(:any)'] = 'RoomBookingController/room_booking_approve';
$route['room-booking-reject/(:any)'] = 'RoomBookingController/room_booking_reject';
//admin
$route['admin'] = 'AdminController';
$route['admin/login'] = 'AuthController/admin_login';
$route['admin/logout'] = 'AuthController/admin_logout';
$route['admin/dashboard'] = 'AdminDashboardController';

$route['admin/dashboard'] = 'AdminDashboardController';
$route['admin/customer-list'] = 'CustomerController/customer_list';

$route['admin/customer/ajax_list'] = 'CustomerController/ajax_list';
$route['admin/customer/ajax_add'] = 'CustomerController/ajax_add';
$route['admin/customer/ajax_update'] = 'CustomerController/ajax_update';
$route['admin/customer/ajax_edit/(:any)'] = 'CustomerController/ajax_edit/$1';
$route['admin/customer/ajax_delete/(:any)'] = 'CustomerController/ajax_delete/$1';

