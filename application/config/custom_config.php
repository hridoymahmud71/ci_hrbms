<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['asset_path'] = BASE_URL."assets/";
$config['site_name'] = "CI Hotel Room Booking";
$config['site_email'] = "no-reply@ci_hrbms.loc";

$config['default_image_placeholder'] = $config['asset_path']."default_images/placeholder.jpg";
$config['default_avatar_image_placeholder'] = $config['asset_path']."default_images/avatar.png";

$config['room_category_image_upload_path'] = FILE_UPLOAD_PATH."room_category/";
$config['room_category_image_source_path'] = FILE_UPLOAD_SOURCE_PATH."room_category/";