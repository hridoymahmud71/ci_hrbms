var save_method; //for save method string
var table;
document.addEventListener('DOMContentLoaded', function () {

    //datatables
    table = $('#table').DataTable({

        processing: true, //Feature control the processing indicator.
        serverSide: true, //Feature control DataTables' server-side processing mode.
        //order: [], //Initial no order.
        order: [[6,'desc']], //last update.

        // Load data for the table's content from an Ajax source
        ajax: {
            url: "booking/ajax-list",
            type: "post",
            complete: function (res) {

            }
        },


        //Set column definition initialisation properties.
        columnDefs: [
            {
                "targets": [-1], //last column
                "orderable": false, //set not orderable
            },
        ],

    });



});



function reload_table() {
    table.ajax.reload(null, false); //reload datatable ajax
}
