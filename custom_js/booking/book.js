document.addEventListener('DOMContentLoaded', function () {


    $('.room_categories').slick({
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 2,
        dots: true,
        arrows: true,
        draggable: false,
        swipe: false,
        swipeToSlide: false,
        touchMove: false,
        draggable: false,
        accessibility: false,
    });


    /*0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000*/

    // ************************************************
// Shopping Cart API
// ************************************************

    var shoppingCart = (function () {
        // =============================
        // Private methods and propeties
        // =============================
        cart = [];

        // Constructor
        function Item(name, key, price, count) {
            this.name = name;
            this.key = key;
            this.price = price;
            this.count = count;
        }

        // Save cart
        function saveCart() {
            sessionStorage.setItem('shoppingCart', JSON.stringify(cart));
        }

        // Load cart
        function loadCart() {
            cart = JSON.parse(sessionStorage.getItem('shoppingCart'));
        }

        if (sessionStorage.getItem("shoppingCart") != null) {
            //loadCart();
        }


        // =============================
        // Public methods and propeties
        // =============================
        var obj = {};

        // Add to cart
        obj.addItemToCart = function (key, name, price, count) {
            for (var item in cart) {
                if (cart[item].key === key) {
                    cart[item].count++;
                    //saveCart;
                    return;
                }
            }
            var item = new Item(name, key, price, count);
            cart.push(item);
            //saveCart;
        }
        // Set count from item
        obj.setCountForItem = function (key, count) {
            for (var i in cart) {
                if (cart[i].key === key) {
                    cart[i].count = count;
                    break;
                }
            }
        };
        // Remove item from cart
        obj.removeItemFromCart = function (key) {
            for (var item in key) {
                if (cart[item].key === key) {
                    cart[item].count--;
                    if (cart[item].count === 0) {
                        cart.splice(item, 1);
                    }
                    break;
                }
            }
            //saveCart;
        }

        // Remove all items from cart
        obj.removeItemFromCartAll = function (key) {
            for (var item in cart) {
                if (cart[item].key === key) {
                    cart.splice(item, 1);
                    break;
                }
            }
            //saveCart;
        }

        // Clear cart
        obj.clearCart = function () {
            cart = [];
            //saveCart;
        }

        // Count cart
        obj.totalCount = function () {
            var totalCount = 0;
            for (var item in cart) {
                totalCount += cart[item].count;
            }
            return totalCount;
        }

        // Total cart
        obj.totalCart = function () {
            var totalCart = 0;
            for (var item in cart) {
                totalCart += cart[item].price * cart[item].count;
            }
            return Number(totalCart.toFixed(2));
        }

        // List cart
        obj.listCart = function () {
            var cartCopy = [];
            for (i in cart) {
                item = cart[i];
                itemCopy = {};
                for (p in item) {
                    itemCopy[p] = item[p];

                }
                itemCopy.total = Number(item.price * item.count).toFixed(2);
                cartCopy.push(itemCopy)
            }
            return cartCopy;
        }

        // cart : Array
        // Item : Object/Class
        // addItemToCart : Function
        // removeItemFromCart : Function
        // removeItemFromCartAll : Function
        // clearCart : Function
        // countCart : Function
        // totalCart : Function
        // listCart : Function
        // saveCart : Function
        // loadCart : Function
        return obj;
    })();


// *****************************************
// Triggers / Events
// *****************************************
// Add item
    $('.add-to-cart').click(function (event) {
        event.preventDefault();
        var name = $(this).data('name');
        var key = $(this).data('key');
        var price = Number($(this).data('price'));
        shoppingCart.addItemToCart(key, name, price, 1);
        displayCart();
    });

// Clear items
    $('.clear-cart').click(function () {
        shoppingCart.clearCart();
        displayCart();
    });


    function displayCart() {
        var cartArray = shoppingCart.listCart();
        var output = "";
        for (var i in cartArray) {
            output += "<tr>"
                + "<td>" + cartArray[i].name + "</td>"
                + "<td>(" + cartArray[i].price + ")</td>"
                + "<td><div class='input-group'><button class='minus-item input-group-addon btn btn-primary' data-key=" + cartArray[i].key + ">-</button>"
                + "<input type='number' class='item-count form-control' data-key='" + cartArray[i].key + "' value='" + cartArray[i].count + "'>"
                + "<button class='plus-item btn btn-primary input-group-addon' data-key=" + cartArray[i].key + ">+</button></div></td>"
                + "<td><button class='delete-item btn btn-danger' data-key=" + cartArray[i].key + ">X</button></td>"
                + " = "
                + "<td>" + cartArray[i].total + "</td>"
                + "</tr>";
        }
        $('.show-cart').html(output);
        $('.total-cart').html(shoppingCart.totalCart());
        $('.total-count').html(shoppingCart.totalCount());
    }

// Delete item button

    $('.show-cart').on("click", ".delete-item", function (event) {
        var key = $(this).data('key');
        shoppingCart.removeItemFromCartAll(key);
        displayCart();
    })


// -1
    $('.show-cart').on("click", ".minus-item", function (event) {
        var key = $(this).data('key');

        shoppingCart.removeItemFromCart(key);
        displayCart();
    })
// +1
    $('.show-cart').on("click", ".plus-item", function (event) {
        var key = $(this).data('key');

        shoppingCart.addItemToCart(key);
        displayCart();
    })

// Item count input
    $('.show-cart').on("change", ".item-count", function (event) {
        var key = $(this).data('key');

        var count = Number($(this).val());
        shoppingCart.setCountForItem(key, count);
        displayCart();
    });

    displayCart();


    /*0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000*/

    var pre_selected_room_category = $("#room_categories").attr('pre_selected_room_category');
    if (pre_selected_room_category != "") {
        if ($("#" + pre_selected_room_category)) {
            $("#" + pre_selected_room_category).click();
        }
    }

    $("#order").on('click', function () {
        var cartArray = shoppingCart.listCart();

        console.log(cartArray);
        order_request(cartArray)
    })

    function order_request(cartArray) {
        var json_cartArray = JSON.stringify(cartArray);

        console.log(json_cartArray);
        request = $.ajax({
            url: "order-request",
            type: "post",
            data: json_cartArray,
            contentType: "application/json; charset=utf-8",
            dataType: "json",

        });

        // Callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR) {
            $("#response-message-error").html("");
            $("#response-message-success").html("");
            console.log(response);

            if (response.success == true) {
                $("#response-message-success").html(response.message);
                if (response.redirect != "") {
                    window.location.href = response.redirect;
                }
            } else if (response.success == false) {
                $("#response-message-error").html(response.message);
            }

        });

        // Callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // Log the error to the console
            console.error(textStatus, errorThrown);
            alert('error');
        });
    }

})

