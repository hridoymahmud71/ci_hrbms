document.addEventListener('DOMContentLoaded', function () {

    $('.summernote').summernote({
        height: 250
    });

    Dropzone.autoDiscover = false;
    var dz_image_upload = new Dropzone(".dropzone", {
        url: $("#room_catgory_image_upload_url").attr('url'),
        maxFilesize: 2,
        maxFiles: 10,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png,.gif,",
        paramName: "image",
        dictInvalidFileType: "This File Type Not Supported",
        addRemoveLinks: true,
        init: function () {
            var count = 0;
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;
                $('#form').append(
                    "<input type='hidden' name='room_category_images[]' value='" + obj + "'>"
                );
                $(".my-dz-message").show();
            });
        }
    });

    $(".image_delete_btn").click(function () {
        if (confirm('Are you sure to delete this?')) {

            var elem = $(this);
            image_delete(elem);
        }
    });

    function image_delete(elem) {

        var  room_category_image_id = elem.attr('room_category_image_id')

        request = $.ajax({
            url: "room-category-image-remove",
            type: "post",
            data: {room_category_image_id: room_category_image_id},

        });

        // Callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR) {
            elem.closest('.image_block').remove();
            console.log("Hooray, it worked!");
        });

        // Callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // Log the error to the console
            console.error(
                "The following error occurred: " +
                textStatus, errorThrown
            );
            alert('error');
        });
    }


})