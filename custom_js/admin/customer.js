var save_method; //for save method string
var table;
document.addEventListener('DOMContentLoaded', function () {

    //extra <starts>
    $('.see_password').hover(function () {
        $('.password').attr('type', 'text');
    }, function () {
        $('.password').attr('type', 'password');
    });
    //extra <ends>


    //datatables
    table = $('#table').DataTable({

        processing: true, //Feature control the processing indicator.
        serverSide: true, //Feature control DataTables' server-side processing mode.
        //order: [], //Initial no order.
        order: [[3,'desc']], //created at.

        // Load data for the table's content from an Ajax source
        ajax: {
            url: "admin/customer/ajax_list",
            type: "post",
            complete: function (res) {

            }
        },


        //Set column definition initialisation properties.
        columnDefs: [
            {
                "targets": [-1], //last column
                "orderable": false, //set not orderable
            },
        ],

    });

    //set input/textarea/select event when change value, remove class error and remove text help block
    $("input").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });


});

function add_customer() {
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('[name="user_email"]').removeAttr('readonly');
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add customer'); // Set Title to Bootstrap modal title
}

function edit_customer(id) {
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url: "admin/customer/ajax_edit/" + id,
        type: "GET",
        dataType: "JSON",
        success: function (data) {

            $('[name="user_id"]').val(data.user_id);
            $('[name="user_name"]').val(data.user_name);
            $('[name="user_email"]').val(data.user_email).attr('readonly', 'readonly');
            $('[name="user_active"]').val(data.user_active).trigger('change');
            $('[name="user_password"]').val();
            $('[name="confirm_password"]').val();

            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit customer'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            alert('Error get data from ajax');
        }
    });
}

function reload_table() {
    table.ajax.reload(null, false); //reload datatable ajax
}

function save() {
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled', true); //set button disable
    var url;

    if (save_method == 'add') {
        url = "admin/customer/ajax_add";
    } else {
        url = "admin/customer/ajax_update";
    }

    // ajax adding data to database
    $.ajax({
        url: url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function (data) {

            if (data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
            else {
                for (var i = 0; i < data.inputerror.length; i++) {
                    $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled', false); //set button enable


        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled', false); //set button enable

        }
    });
}

function delete_customer(id) {
    if (confirm('Are you sure delete this data?')) {
        // ajax delete data to database
        $.ajax({
            url: "admin/customer/ajax_delete/" + id,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error deleting data');
            }
        });

    }
}
