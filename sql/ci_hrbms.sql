-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2019 at 11:19 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_hrbms`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking_order`
--

CREATE TABLE `booking_order` (
  `booking_order_id` bigint(11) NOT NULL,
  `booking_order_key` varchar(255) DEFAULT NULL,
  `user_id` bigint(11) NOT NULL,
  `booking_order_amount` decimal(25,2) NOT NULL,
  `booking_order_is_paid` int(11) NOT NULL,
  `booking_order_paid_at` timestamp NULL DEFAULT NULL,
  `booking_order_approval` tinyint(4) NOT NULL,
  `booking_order_created_at` timestamp NULL DEFAULT NULL,
  `booking_order_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking_order`
--

INSERT INTO `booking_order` (`booking_order_id`, `booking_order_key`, `user_id`, `booking_order_amount`, `booking_order_is_paid`, `booking_order_paid_at`, `booking_order_approval`, `booking_order_created_at`, `booking_order_updated_at`) VALUES
(1, 'bk_ord_00b574e99292de153d78', 2, '160.00', 1, '2019-01-17 14:32:30', 1, '2019-01-17 14:31:58', '2019-01-17 22:01:06'),
(2, 'bk_ord_f02a467842f5bd617a47', 7, '75.00', 1, '2019-01-17 19:31:31', 0, '2019-01-17 19:27:25', '2019-01-17 19:31:31');

-- --------------------------------------------------------

--
-- Table structure for table `booking_order_room_category`
--

CREATE TABLE `booking_order_room_category` (
  `booking_order_room_category_id` bigint(20) NOT NULL,
  `room_category_id` bigint(20) NOT NULL,
  `booking_order_id` bigint(20) NOT NULL,
  `room_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking_order_room_category`
--

INSERT INTO `booking_order_room_category` (`booking_order_room_category_id`, `room_category_id`, `booking_order_id`, `room_count`) VALUES
(1, 1, 1, 2),
(2, 2, 1, 2),
(3, 2, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `paypal_transaction`
--

CREATE TABLE `paypal_transaction` (
  `paypal_transaction_id` bigint(20) NOT NULL,
  `booking_order_key` varchar(255) DEFAULT NULL,
  `user_key` varchar(255) DEFAULT NULL,
  `txn_id` varchar(255) NOT NULL,
  `payment_gross` decimal(25,2) NOT NULL DEFAULT '0.00',
  `currency_code` varchar(255) DEFAULT NULL,
  `payer_email` varchar(255) DEFAULT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `paypal_transaction`
--

INSERT INTO `paypal_transaction` (`paypal_transaction_id`, `booking_order_key`, `user_key`, `txn_id`, `payment_gross`, `currency_code`, `payer_email`, `payment_status`) VALUES
(1, 'bk_ord_00b574e99292de153d78', 'usr_33f6615d26c9dd849b72', '3DY01425T54800617', '160.00', 'USD', 'demoemailacc.xclient@gmail.com', 'Completed'),
(2, 'bk_ord_f02a467842f5bd617a47', 'usr_fdd39ceac444ae7e8c41', '66J25462K1822915U', '75.00', 'USD', 'demoemailacc.rsadm@gmail.com', 'Completed');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  `role_title` text,
  `role_deletable` tinyint(4) NOT NULL,
  `role_reated_at` timestamp NULL DEFAULT NULL,
  `role_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `role`, `role_title`, `role_deletable`, `role_reated_at`, `role_updated_at`) VALUES
(1, 'admin', 'Admin', 0, '2019-01-12 18:00:00', NULL),
(2, 'customer', 'Customer', 0, '2019-01-12 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `room_category`
--

CREATE TABLE `room_category` (
  `room_category_id` bigint(20) NOT NULL,
  `room_category_key` varchar(255) DEFAULT NULL,
  `room_category_name` varchar(255) DEFAULT NULL,
  `room_category_description` text,
  `room_category_price` decimal(25,2) NOT NULL DEFAULT '0.00',
  `total_number_of_rooms_in_category` int(11) NOT NULL,
  `booked_rooms_in_category` int(11) NOT NULL,
  `room_category_deleted` tinyint(4) NOT NULL,
  `room_category_created_at` timestamp NULL DEFAULT NULL,
  `room_category_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `room_category`
--

INSERT INTO `room_category` (`room_category_id`, `room_category_key`, `room_category_name`, `room_category_description`, `room_category_price`, `total_number_of_rooms_in_category`, `booked_rooms_in_category`, `room_category_deleted`, `room_category_created_at`, `room_category_updated_at`) VALUES
(1, 'rm_cat_5e196415b1424e4208f8', 'dgdfgfdgfdg cc', 'fgdfgd fgdg dg<br>', '55.00', 4, 2, 0, '2019-01-15 03:18:59', '2019-01-17 22:01:06'),
(2, 'rm_cat_6316f561e89bf5e7c6a1', 'SIngl master', '<p>&nbsp;ssdfd df sd sdsd fsdf sdfsd fsdf </p><p>&nbsp;aeae wqeq eqwe qw eqw e<br>&nbsp; werewr<br></p>', '25.00', 44, 2, 0, '2019-01-15 05:27:35', '2019-01-17 22:01:06'),
(3, 'rm_cat_b8f7e9fcb3d469946ba4', 'erae', 'da dad sadasd <br>', '43.00', 22, 0, 0, '2019-01-15 08:53:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `room_category_image`
--

CREATE TABLE `room_category_image` (
  `room_category_image_id` bigint(20) NOT NULL,
  `room_category_id` int(11) NOT NULL,
  `room_category_image_name` text,
  `is_image_featured` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `room_category_image`
--

INSERT INTO `room_category_image` (`room_category_image_id`, `room_category_id`, `room_category_image_name`, `is_image_featured`) VALUES
(1, 2, 'd624c8da6e67fc941ba55ec9c3fd2f9b.jpg', 0),
(2, 2, '0a4c1266537e4f68a5a8c79397b88edf.PNG', 0),
(3, 3, '361e1c12e0b3d2889ade3948faa86a4b.PNG', 0),
(4, 3, '76138e006538fa203eb5c538589b1304.PNG', 0),
(5, 2, 'f92f38e40b3b2dd6f2b8afc03dc2563d.PNG', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` bigint(11) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_password` text,
  `user_active` tinyint(4) NOT NULL,
  `user_deleted` tinyint(4) NOT NULL,
  `user_key` text,
  `user_created_at` timestamp NULL DEFAULT NULL,
  `user_updated_at` timestamp NULL DEFAULT NULL,
  `user_password_reset_requested_at` timestamp NULL DEFAULT NULL,
  `user_password_reset_code` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_email`, `user_password`, `user_active`, `user_deleted`, `user_key`, `user_created_at`, `user_updated_at`, `user_password_reset_requested_at`, `user_password_reset_code`) VALUES
(1, 'Adm Mahmud', 'hridoymahmud71@gmail.com', '$2y$10$pCm9mRdzYHnf5PuG0wiweeEAy57fKW/IYrUJ.YLqYaVE8R5LmCfgy', 1, 0, 'usr_e573833911de3bc963dd', '2019-01-13 20:52:18', NULL, NULL, NULL),
(2, 'Demo X', 'demoemailacc.xclient@gmail.com', '$2y$10$LZQyjFcbDswSmBWYGvr94urme1uOCEHOqRhWWZmdBK7NxQQ7SFLs.', 1, 0, 'usr_33f6615d26c9dd849b72', '2019-01-13 23:22:16', NULL, NULL, NULL),
(3, 'sample man', 'sample@man.com', '$2y$10$g1L42WqmflUDU2ycdfaa5Op89rNc6If/JlHCZTFB9Vo7Wo/PSRAc6', 1, 0, 'usr_bece9c1f730dc41f938f', '2019-01-14 11:25:57', NULL, NULL, NULL),
(4, 'sqq', 'sqa@man.com', '$2y$10$Q3.YNGCgtMB5aei1TokOkeNSUsv3IM6Ll8wKByPhXK61jpDEUAgwW', 0, 0, 'usr_f8b0062616e72e4c48dd', '2019-01-14 11:27:56', '2019-01-14 11:31:54', NULL, NULL),
(5, 'DemoY', 'demoemailacc.yclient@demo.com', '$2y$10$.vsNUeG/jKIkClvdm6NUfuLLsEC4sW9Qts.Au4kpHD7xUZK0Sj2VC', 0, 0, 'usr_28780d578f21bf4abf76', '2019-01-17 19:06:52', NULL, NULL, NULL),
(6, 'Demo RS', 'demoemailacc.rsadm@demo.com', '$2y$10$VZG1zzY9X68qIaRA3oiYP.ZEetAxxKlzuNrGJ/uLg.UjuQRcpVSfG', 0, 0, 'usr_321966ce8a2c630fad5a', '2019-01-17 19:14:56', NULL, NULL, NULL),
(7, 'Demo RS', 'demoemailacc.rsadm@gmail.com', '$2y$10$jvoWEmn4SFFXnCcIcJtJVu1PBa5l21LLsSVgCBQ/m7Mc.RcFfAM2m', 1, 0, 'usr_fdd39ceac444ae7e8c41', '2019-01-17 19:21:14', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_activation`
--

CREATE TABLE `user_activation` (
  `user_activation_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_activation_code` text,
  `user_activation_code_expires` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_activation`
--

INSERT INTO `user_activation` (`user_activation_id`, `user_id`, `user_activation_code`, `user_activation_code_expires`) VALUES
(1, 5, 'usactv_1c3c6a89da8f562b89b5', '2019-01-18 23:00:00'),
(2, 6, 'usactv_7eb1fb1e8d4154d8c18f', '2019-01-18 23:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `user_role_id` bigint(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`user_role_id`, `user_id`, `role_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 2),
(4, 4, 2),
(5, 5, 2),
(6, 6, 2),
(7, 7, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking_order`
--
ALTER TABLE `booking_order`
  ADD PRIMARY KEY (`booking_order_id`);

--
-- Indexes for table `booking_order_room_category`
--
ALTER TABLE `booking_order_room_category`
  ADD PRIMARY KEY (`booking_order_room_category_id`);

--
-- Indexes for table `paypal_transaction`
--
ALTER TABLE `paypal_transaction`
  ADD PRIMARY KEY (`paypal_transaction_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `room_category`
--
ALTER TABLE `room_category`
  ADD PRIMARY KEY (`room_category_id`);

--
-- Indexes for table `room_category_image`
--
ALTER TABLE `room_category_image`
  ADD PRIMARY KEY (`room_category_image_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_activation`
--
ALTER TABLE `user_activation`
  ADD PRIMARY KEY (`user_activation_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`user_role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking_order`
--
ALTER TABLE `booking_order`
  MODIFY `booking_order_id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `booking_order_room_category`
--
ALTER TABLE `booking_order_room_category`
  MODIFY `booking_order_room_category_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `paypal_transaction`
--
ALTER TABLE `paypal_transaction`
  MODIFY `paypal_transaction_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `room_category`
--
ALTER TABLE `room_category`
  MODIFY `room_category_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `room_category_image`
--
ALTER TABLE `room_category_image`
  MODIFY `room_category_image_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user_activation`
--
ALTER TABLE `user_activation`
  MODIFY `user_activation_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `user_role_id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
